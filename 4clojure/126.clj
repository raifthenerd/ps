;; Enter a value which satisfies the following:

(def __
  java.lang.Class
  )

(let [x __]
  (and (= (class x) x) x))
