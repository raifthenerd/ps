;; Write a function which removes the duplicates from a sequence. Order of the items must be maintained.
;;
;; Restrictions (DON'T use these function(s)): distinct

(def __
  (partial
   (fn [xs ys]
     (if (empty? ys)
       xs
       (if (some #(= (first ys) %) xs)
         (recur xs (rest ys))
         (recur (conj xs (first ys)) (rest ys)))))
   [])
  )

(= (__ [1 2 1 3 1 2 4]) [1 2 3 4])

(= (__ [:a :a :b :b :c :c]) [:a :b :c])

(= (__ '([2 4] [1 2] [1 3] [1 3])) '([2 4] [1 2] [1 3]))

(= (__ (range 50)) (range 50))
