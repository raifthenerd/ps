;; Create a function of two integer arguments: the start and end, respectively.  You must create a vector of strings which renders a 45&deg; rotated square of integers which are successive squares from the start point up to and including the end point.  If a number comprises multiple digits, wrap them around the shape individually.  If there are not enough digits to complete the shape, fill in the rest with asterisk characters.  The direction of the drawing should be clockwise, starting from the center of the shape and working outwards, with the initial direction being down and to the right.

(def __
  (fn [start end]
    (let [ns (->> start
                  (iterate #(* % %))
                  (take-while (partial >= end))
                  (map str)
                  (apply concat))
          n (first (drop-while #(< (* % %) (count ns)) (range)))
          base [(* 2 (quot (dec n) 2)) (dec n)]
          board (nth (iterate #(into [] (repeat (dec (* 2 n)) %)) \space) 2)
          moves [(fn [[y x]] [(inc y) (inc x)])
                 (fn [[y x]] [(inc y) (dec x)])
                 (fn [[y x]] [(dec y) (dec x)])
                 (fn [[y x]] [(dec y) (inc x)])]
          movement (drop-last
                    (apply concat
                           (map #(repeat (inc (quot % 2)) (rem % 4))
                                (drop-last (range (* 2 n))))))
          aux (fn [board loc ns ms]
                (if (empty? ms)
                  board
                  (let [loc ((first ms) loc)]
                    (recur (assoc-in board loc (first ns))
                           loc
                           (rest ns)
                           (rest ms)))))]
      (->> (aux (assoc-in board base (first ns))
                base
                (concat (rest ns) (repeat \*))
                (map moves movement))
           (map (partial apply str))
           (into []))
      ))
  )

(= (__ 2 2) ["2"])

(= (__ 2 4) [" 2 "
             "* 4"
             " * "])

(= (__ 3 81) [" 3 "
              "1 9"
              " 8 "])

(= (__ 4 20) [" 4 "
              "* 1"
              " 6 "])

(= (__ 2 256) ["  6  "
               " 5 * "
               "2 2 *"
               " 6 4 "
               "  1  "])

(= (__ 10 10000) ["   0   "
                  "  1 0  "
                  " 0 1 0 "
                  "* 0 0 0"
                  " * 1 * "
                  "  * *  "
                  "   *   "])
