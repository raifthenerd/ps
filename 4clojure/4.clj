;; Lists can be constructed with either a function or a quoted form.

(def __
  :a :b :c
  )

(= (list __) '(:a :b :c))
