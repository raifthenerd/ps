;; Given an input sequence of keywords and numbers, create a map such that each key in the map is a keyword, and the value is a sequence of all the numbers (if any) between it and the next keyword in the sequence.

(def __
  (partial
   (fn [out [x & xs]]
     (if x
       (let [[col res] (split-with #(not (keyword? %)) xs)]
         (recur (assoc out x col) res))
       out))
   {})
  )

(= {} (__ []))

(= {:a [1]} (__ [:a 1]))

(= {:a [1], :b [2]} (__ [:a 1, :b 2]))

(= {:a [1 2 3], :b [], :c [4]} (__ [:a 1 2 3 :b :c 4]))
