;; Given a string of comma separated integers, write a function which returns a new comma separated string that only contains the numbers which are perfect squares.

(def __
  (fn [s]
    (->> (str "[" s "]")
         read-string
         (filter (fn [n] (= n (#(* % %) (int (Math/sqrt n))))))
         (interpose ",")
         (apply str)))
  )

(= (__ "4,5,6,7,8,9") "4,9")

(= (__ "15,16,25,36,37") "16,25,36")
