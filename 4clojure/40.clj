;; Write a function which separates the items of a sequence by an arbitrary value.
;;
;; Restrictions (DON'T use these function(s)): interpose

(def __
  (fn myinterpose [x [y & ys]]
    (cons y (if ys (cons x (myinterpose x ys)))))
  )

(= (__ 0 [1 2 3]) [1 0 2 0 3])

(= (apply str (__ ", " ["one" "two" "three"])) "one, two, three")

(= (__ :z [:a :b :c :d]) [:a :z :b :z :c :z :d])
