;; Starting with a graph you must write a function that returns true if it is possible to make a tour of the graph in which every edge is visited exactly once.<br/><br/>The graph is represented by a vector of tuples, where each tuple represents a single edge.<br/><br/>The rules are:<br/><br/>- You can start at any node.<br/>- You must visit each edge exactly once.</br>- All edges are undirected.

(def __
  (fn [xs]
    (let [node-info (reduce
                     (fn [m [x y]]
                       (-> m
                           (update-in [x] (fnil (partial cons y) '()))
                           (update-in [y] (fnil (partial cons x) '()))))
                     {} xs)]
      (and (= (->> node-info keys (into #{}))
              ((fn [s]
                 (if (empty? s)
                   (recur #{(-> node-info keys first)})
                   (let [res (reduce
                              (fn [m a]
                                (->> a
                                     (get node-info)
                                     (concat m)
                                     (into #{}))) s s)]
                     (if (= (count s) (count res))
                       res
                       (recur res))))) #{}))
           (->> node-info
                vals
                (filter #(odd? (count %)))
                count
                (contains? #{0 2})))))
  )

(= true (__ [[:a :b]]))

(= false (__ [[:a :a] [:b :b]]))

(= false (__ [[:a :b] [:a :b] [:a :c] [:c :a]
               [:a :d] [:b :d] [:c :d]]))

(= true (__ [[1 2] [2 3] [3 4] [4 1]]))

(= true (__ [[:a :b] [:a :c] [:c :b] [:a :e]
              [:b :e] [:a :d] [:b :d] [:c :e]
              [:d :e] [:c :f] [:d :f]]))

(= false (__ [[1 2] [2 3] [2 4] [2 5]]))
