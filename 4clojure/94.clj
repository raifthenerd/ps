;; The <a href="http://en.wikipedia.org/wiki/Conway's_Game_of_Life">game of life</a> is a cellular automaton devised by mathematician John Conway. <br/><br/>The 'board' consists of both live (#) and dead ( ) cells. Each cell interacts with its eight neighbours (horizontal, vertical, diagonal), and its next state is dependent on the following rules:<br/><br/>1) Any live cell with fewer than two live neighbours dies, as if caused by under-population.<br/>2) Any live cell with two or three live neighbours lives on to the next generation.<br/>3) Any live cell with more than three live neighbours dies, as if by overcrowding.<br/>4) Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.<br/><br/>Write a function that accepts a board, and returns a board representing the next generation of cells.

(def __
  (let [delta
        (reduce (fn [m1 k1]
                  (concat m1 (reduce (fn [m2 k2]
                                       (if (and (zero? k1) (zero? k2))
                                         m2 (conj m2 [k1 k2])))
                                     '() [-1 0 1])))
                '() [-1 0 1])]
    (fn [cells]
      (let [rows (count cells)
            cols (count (first cells))
            lives
            (reduce-kv (fn [m1 k1 v1]
                         (concat m1 (reduce-kv (fn [m2 k2 v2]
                                                 (if (= v2 \#)
                                                   (conj m2 [k1 k2]) m2))
                                               '() (zipmap (range cols) v1))))
                       '() (zipmap (range rows) cells))
            neighbors (frequencies
                       (mapcat (fn [[x y]]
                                 (map (fn [[dx dy]]
                                        [(+ x dx) (+ y dy)])
                                      delta))
                               lives))
            next-step (reduce-kv (fn [m k v]
                                   (if (= v 3) (conj m k) m))
                                 (reduce (fn [m k]
                                           (if (= (get neighbors k) 2)
                                             (conj m k) m))
                                         #{} lives) neighbors)
            world (reduce
                   (fn [m k] (update-in m k inc))
                   (->> 0 (repeat cols) (into [])
                        (repeat rows) (into [])) next-step)]
        (map (partial apply str)
             (map (partial map [\space \#]) world)))))
  )

(= (__ ["      "  
        " ##   "
        " ##   "
        "   ## "
        "   ## "
        "      "])
   ["      "  
    " ##   "
    " #    "
    "    # "
    "   ## "
    "      "])

(= (__ ["     "
        "     "
        " ### "
        "     "
        "     "])
   ["     "
    "  #  "
    "  #  "
    "  #  "
    "     "])

(= (__ ["      "
        "      "
        "  ### "
        " ###  "
        "      "
        "      "])
   ["      "
    "   #  "
    " #  # "
    " #  # "
    "  #   "
    "      "])
