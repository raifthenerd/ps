;; <p>Following on from <a href="http://www.4clojure.com/problem/128">Recognize Playing Cards</a>, determine the best poker hand that can be made with five cards. The hand rankings are listed below for your convenience.</p>
;;
;; <ol>
;;
;; <li>Straight flush: All cards in the same suit, and in sequence</li>
;;
;; <li>Four of a kind: Four of the cards have the same rank</li>
;;
;; <li>Full House: Three cards of one rank, the other two of another rank</li>
;;
;; <li>Flush: All cards in the same suit</li>
;;
;; <li>Straight: All cards in sequence (aces can be high or low, but not both at once)</li>
;;
;; <li>Three of a kind: Three of the cards have the same rank</li>
;;
;; <li>Two pair: Two pairs of cards have the same rank</li>
;;
;; <li>Pair: Two cards have the same rank</li>
;;
;; <li>High card: None of the above conditions are met</li>
;;
;; </ol>

(def __
  (fn [hands]
    (let [hands (map (fn [[suit rank]] (assoc {}
                                              :suit (case suit
                                                      \S :spade \D :diamond
                                                      \H :heart \C :club)
                                              :rank (case rank
                                                      \T 8 \J 9 \Q 10 \K 11 \A 12
                                                      (- (int rank) (int \2))))) hands)
          same-suit (apply = (map :suit hands))
          ranks (sort (map :rank hands))
          is-straight (or (= ranks '(0 1 2 3 12))
                          (every? (partial = 1)
                                  (map - (rest ranks) (drop-last ranks))))
          ranks (sort
                 (remove (partial = 1)
                         (vals (reduce (fn [ranks rank]
                                         (assoc ranks rank (inc (get ranks rank 0))))
                                       {} ranks))))]

      (cond
        (and same-suit is-straight) :straight-flush
        (= ranks '(4)) :four-of-a-kind
        (= ranks '(2 3)) :full-house
        same-suit :flush
        is-straight :straight
        (= ranks '(3)) :three-of-a-kind
        (= ranks '(2 2)) :two-pair
        (= ranks '(2)) :pair
        :else :high-card)
      ))
  )

(= :high-card (__ ["HA" "D2" "H3" "C9" "DJ"]))

(= :pair (__ ["HA" "HQ" "SJ" "DA" "HT"]))

(= :two-pair (__ ["HA" "DA" "HQ" "SQ" "HT"]))

(= :three-of-a-kind (__ ["HA" "DA" "CA" "HJ" "HT"]))

(= :straight (__ ["HA" "DK" "HQ" "HJ" "HT"]))

(= :straight (__ ["HA" "H2" "S3" "D4" "C5"]))

(= :flush (__ ["HA" "HK" "H2" "H4" "HT"]))

(= :full-house (__ ["HA" "DA" "CA" "HJ" "DJ"]))

(= :four-of-a-kind (__ ["HA" "DA" "CA" "SA" "DJ"]))

(= :straight-flush (__ ["HA" "HK" "HQ" "HJ" "HT"]))
