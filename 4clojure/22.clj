;; Write a function which returns the total number of elements in a sequence.
;;
;; Restrictions (DON'T use these function(s)): count

(def __
  reduce #(if (nil? %2) % (inc %)) 0
  )

(= (__ '(1 2 3 3 1)) 5)

(= (__ "Hello World") 11)

(= (__ [[1 2] [3 4] [5 6]]) 3)

(= (__ '(13)) 1)

(= (__ '(:a :b :c)) 3)
