;; Every node of a tree is connected to each of its children as
;;
;; well as its parent.  One can imagine grabbing one node of
;;
;; a tree and dragging it up to the root position, leaving all
;;
;; connections intact.  For example, below on the left is
;;
;; a binary tree.  By pulling the "c" node up to the root, we
;;
;; obtain the tree on the right.
;;
;; <br/>
;;
;; <img src="http://i.imgur.com/UtD2T.png">
;;
;; <br/>
;;
;; Note it is no longer binary as "c" had three connections
;;
;; total -- two children and one parent.
;;
;; Each node is represented as a vector, which always has at
;;
;; least one element giving the name of the node as a symbol.
;;
;; Subsequent items in the vector represent the children of the
;;
;; node.  Because the children are ordered it's important that
;;
;; the tree you return keeps the children of each node in order
;;
;; and that the old parent node, if any, is appended on the
;;
;; right.
;;
;; Your function will be given two args -- the name of the node
;;
;; that should become the new root, and the tree to transform.

(def __
  (fn [newroot tree]
    (let [move-down
          (fn [[tree zipper]]
            (if-not (empty? (rest tree))
              (list (first (rest tree))
                    {:name (first tree)
                     :left '()
                     :right (rest (rest tree))
                     :zipper zipper})))
          move-up
          (fn [[tree zipper]]
            (if-not (empty? zipper)
              (list (cons (zipper :name)
                          (reverse (cons tree (zipper :left))))
                    (zipper :zipper))))
          move-right
          (fn [[tree zipper]]
            (if-not (empty? (zipper :right))
              (list (first (zipper :right))
                    (assoc zipper
                           :left (cons tree (zipper :left))
                           :right (rest (zipper :right))))))
          search
          (fn [x]
            (if (= newroot (first (first x)))
              x
              (if-let [x (move-down x)]
                (recur x)
                (if-let [x (move-right x)]
                  (recur x)
                  (recur (->> x
                              (iterate move-up)
                              (drop-while (comp nil? move-right))
                              first
                              move-right))))))
          add-right
          (fn [root child]
            (if (nil? child) root
                (if (nil? root) child
                    (->> root reverse (cons child) reverse))))
          construct
          (fn [zip] (cons (zip :name) (concat (reverse (zip :left)) (zip :right))))
          parse
          (fn [tree zipper]
            (let [aux (fn [tree zips]
                        (if (empty? (first zips))
                          tree
                          (recur (add-right tree (construct (first zips)))
                                 (rest zips))))]
              (add-right tree (aux nil (iterate :zipper zipper)))))]
      (apply parse (search (list tree {})))))
  )

(= '(n)
   (__ 'n '(n)))

(= '(a (t (e)))
   (__ 'a '(t (e) (a))))

(= '(e (t (a)))
   (__ 'e '(a (t (e)))))

(= '(a (b (c)))
   (__ 'a '(c (b (a)))))

(= '(d 
      (b
        (c)
        (e)
        (a 
          (f 
            (g) 
            (h)))))
  (__ 'd '(a
            (b 
              (c) 
              (d) 
              (e))
            (f 
              (g)
              (h)))))

(= '(c 
      (d) 
      (e) 
      (b
        (f 
          (g) 
          (h))
        (a
          (i
          (j
            (k)
            (l))
          (m
            (n)
            (o))))))
   (__ 'c '(a
             (b
               (c
                 (d)
                 (e))
               (f
                 (g)
                 (h)))
             (i
               (j
                 (k)
                 (l))
               (m
                 (n)
                 (o))))))
