;; Given a vector of integers, find the longest consecutive sub-sequence of increasing numbers. If two sub-sequences have the same length, use the one that occurs first. An increasing sub-sequence must have a length of 2 or greater to qualify.

(def __
  (fn foo [coll]
    (let [increase? (fn [xs]
                      (if (<= (count xs) 1)
                        true
                        (and (< (first xs) (second xs)) (recur (rest xs)))))
          longer (fn [xs ys] (if (>= (count xs) (count ys)) xs ys))]
      (if (> (count coll) 1)
        (let [div (int (/ (inc (count coll)) 2))
              [head tail] (map vec (split-at div coll))
              start (some #(if (increase? (subvec coll % (inc div))) %)
                          (range div))
              end (some #(if (increase? (subvec coll div %)) %)
                        (map inc (reverse (drop div (range (count coll))))))
              sub-mid (if (and start end) (subvec coll start end))]
          (longer (longer (foo head) (foo tail)) sub-mid))
        [])))
  )

(= (__ [1 0 1 2 3 0 4 5]) [0 1 2 3])

(= (__ [5 6 1 3 2 7]) [5 6])

(= (__ [2 3 3 4 5]) [3 4 5])

(= (__ [7 6 5 4]) [])
