;; When parsing a snippet of code it's often a good idea to do a sanity check to see if all the brackets match up. Write a function that takes in a string and returns truthy if all square [ ] round ( ) and curly { } brackets are properly paired and legally nested, or returns falsey otherwise.

(def __
  (partial
   (fn [stack string]
     (let [token (first string)
           string (rest string)]
       (cond
         (nil? token) (empty? stack)
         (some #(= % token) "[{(") (recur (conj stack token) string)
         (some #(= % token) ")}]") (and (= (get {\( \) \{ \} \[ \]} (peek stack))
                                           token)
                                        (recur (rest stack) string))
         :else (recur stack string))))
   '())
  )

(__ "This string has no brackets.")

(__ "class Test {
      public static void main(String[] args) {
        System.out.println(\"Hello world.\");
      }
    }")

(not (__ "(start, end]"))

(not (__ "())"))

(not (__ "[ { ] } "))

(__ "([]([(()){()}(()(()))(([[]]({}()))())]((((()()))))))")

(not (__ "([]([(()){()}(()(()))(([[]]({}([)))())]((((()()))))))"))

(not (__ "["))
