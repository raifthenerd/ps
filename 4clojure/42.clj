;; Write a function which calculates factorials.

(def __
  #(reduce * (take % (iterate inc 1)))
  )

(= (__ 1) 1)

(= (__ 3) 6)

(= (__ 5) 120)

(= (__ 8) 40320)
