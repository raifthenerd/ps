% !TEX encoding = UTF-8 Unicode
% !TEX TS-program = lualatex
% !BIB TS-program = bibtex

\PassOptionsToPackage{no-math}{fontspec}
\documentclass[a4paper,9pt,article,adjustmath,figtabcapt,footnote,nokorean,twocolumn]{oblivoir}
\usepackage{fapapersize}
\usefapapersize{*,*,0.2in,*,0.2in,*}

\usepackage{microtype}
\DisableLigatures{encoding=*,family=tt*}
\usepackage{amsmath,amssymb,amsfonts,amsthm,mathtools,mathrsfs}
\usepackage[largesc,theoremfont]{newtxtext}
\usepackage{newtxmath}
\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\usepackage{graphicx}
\graphicspath{{./figures/}}
\usepackage{algorithm,algpseudocode}
\usepackage[cache=false]{minted}
\renewcommand{\listingscaption}{Snippet}
\usemintedstyle{pastie}
\setminted{fontsize=\footnotesize}
\usepackage{booktabs}
\usepackage[inline]{enumitem}
\usepackage[sort&compress]{natbib}
\bibliographystyle{chicago}
\setlength{\textfloatsep}{0pt}
\setlength{\floatsep}{0pt}
\setlength{\intextsep}{0.25em}

\begin{document}\pagestyle{empty}
\section{Setup Environment}
\label{sec:setup}
\begin{listing}[H]
\caption{Bash script}
\begin{minted}{bash}
#!/usr/bin/env bash

SRCDIR='cpp'
CXX='g++'
CXXFLAGS='-lm -lcrypt -O2 -std=c++11 -pipe -DONLINE_JUDGE'
TEST_INPUT='in.txt'
TEST_OUTPUT='out.txt'
TMP=$(mktemp --suffix=.txt)

if [ $# != 1 ]; then
    echo "usage: $0 prob_num"; exit 1
else
    EXEC=$(mktemp); $CXX $CXXFLAGS -o $EXEC $SRCDIR/$2.cpp
    time $EXEC <$TEST_INPUT >$TMP; rm -f $EXEC
fi
diff $TEST_OUTPUT $TMP; rm -f $TMP
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{C++11 template}
\begin{minted}{c++}
#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Emacs configuration}
\begin{minted}{emacs}
;;; init.el
(setq-default
 use-dialog-box nil
 case-fold-search t
 make-backup-files nil
 auto-save-default t
 electric-pair-pairs '((?\" . ?\") (?\{ . ?\}))
 show-paren-style 'expression
 show-paren-delay 0
 c-basic-offset 2
 c-default-style "bsd"
 c-macro-prompt-flag t
 gdb-many-windows t
 gdb-show-main t)
(define-key global-map (kbd "RET") 'newline-and-indent)
(global-hl-line-mode 1)
(global-subword-mode t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(electric-pair-mode 1)
(show-paren-mode t)
\end{minted}
\end{listing}
\section{Data Structures}
\label{sec:data-struct}
\subsection{Basic Data Structures}
\label{sec:basic-data-struct}
\begin{table}[H]
  \centering\scriptsize
  \caption{Basic properties of C++11 STL containers}
  \begin{tabular}{@{}lllll@{}}
    \toprule
    \textbf{C++11 STL}                & \multicolumn{3}{l}{\textbf{Time Complexity}}               & \textbf{Persistent Iterators} \\\cmidrule(lr){2-4}
                                      & \textbf{Access}  & \textbf{Insert / Erase} & \textbf{Find} &                               \\\midrule
    \texttt{array}                    & $O(1)$           & ---                & Sorted: $O(\log n)$  & ---                             \\
                                      &                  &                  & Other: $O(n)$        &                               \\\cmidrule{1-5}
    \texttt{vector} / \texttt{string} & $O(1)$           & Back: $O(1)$     & Sorted: $O(\log n)$  & No                            \\
                                      &                  & Other: $O(n)$    & Other: $O(n)$        &                               \\\cmidrule{1-5}
    \texttt{deque}                    & $O(1)$           & Back: $O(1)$     & Sorted: $O(\log n)$  & Pointers only                 \\
                                      &                  & Other: $O(n)$    & Other: $O(n)$        &                               \\\cmidrule{1-5}
    \texttt{list}                     & Back: $O(1)$     & Back: $O(1)$     & $O(n)$               & Yes                           \\
                                      & Iterator: $O(1)$ & Iterator: $O(1)$ &                      &                               \\
                                      & Other: $O(n)$    & Other: $O(n)$    &                      &                               \\\cmidrule{1-5}
    \texttt{set} / \texttt{map}       & $O(\log n)$      & $O(\log n)$      & $O(\log n)$          & Yes                           \\\cmidrule{1-5}
    \texttt{unordered\_set} /         & Average: $O(1)$  & Average: $O(1)$  & Average: $O(1)$      & Pointers only                 \\
    \texttt{unordered\_map}           & Worst: $O(n)$    & Worst: $O(n)$    & Worst: $O(n)$        &                               \\\cmidrule{1-5}
    \texttt{stack} / \texttt{queue}   & $O(1)$           & $O(1)$           & ---                    & ---                             \\\cmidrule{1-5}
    \texttt{priority\_queue}          & $O(1)$           & $O(\log n)$      & ---                    & ---                             \\\bottomrule
  \end{tabular}
\end{table}
\subsection{Policy-based Data Structures in \texttt{g++}}
\label{sec:policy-based-data}
\begin{listing}[H]
\caption{Order-statistics tree}
\begin{minted}{c++}
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/tree_policy.hpp>
using namespace __gnu_pbds;

template<typename K = int, typename V = null_type>
using Tree = tree<K, V, less<K>, rb_tree_tag,
  tree_order_statistics_node_update>;

void example() {
  Tree<int> s1, s2; s1.insert(55); s1.insert(30);
  assert(*s.find_by_order(0) == 30); assert(s.find_by_order(2) == s.end());
  assert(s.order_of_key(10) == 0); assert(s.order_of_key(30) == 1);
  s2.insert(100); s2.insert(101); s1.join(s2); // s1 > s2 or s1 < s2
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Trie}
\begin{minted}{c++}
#include <ext/pb_ds/assoc_container.hpp>
#include <ext/pb_ds/trie_policy.hpp>
using namespace __gnu_pbds;

template<typename K = vector<int>, typename K::value_type K_min = 0,
         typename K::value_type K_max = 26, class V = null_type>
using Trie = trie<K, V, trie_string_access_traits<K, K_min, K_max>,
  pat_trie_tag, trie_prefix_search_node_update>;

void print_prefix_match(const Trie<string, 'a', 'z'>& t, const string& key) {
  typedef Trie<string, 'a', 'z'>::const_iterator const_iterator;
  typedef pair<const_iterator, const_iterator> 	pair_type;
  const pair_type match_range = t.prefix_range(key);
  for (const_iterator it = match_range.first; it != match_range.second; ++it)
    cout << *it << ' ';
  cout << '\n';
}

void example() {
  Trie<string, 'a', 'z'> t;
  assert(t.insert("wish").second == true);
  assert(t.insert("wish").second == false);
}
\end{minted}
\end{listing}
\subsection{Fenwick Tree}
\label{sec:fenwick-tree}
\begin{listing}[H]
\caption{Fenwick tree for 2D range sum query}
\begin{minted}{c++}
template<typename V = int>
class FenwickTree {
 private:
  vector<vector<V>> segment;
  int LSOne(int S) { return S & (-S); }
  void update(V val, int x, int y) {  // Update [1, x] x [1, y] with val
    for (; x && x < SZ(segment); x += LSOne(x))
      for (int tmp = y; tmp && tmp < SZ(segment[x]); tmp += LSOne(tmp))
        segment[x][tmp] += val;
  }
  V query(int x, int y) {  // Get range sum of [1, x] x [1, y]
    V sum = 0;
    for (; x; x -= LSOne(x))
      for (int tmp = y; tmp; tmp -= LSOne(tmp))
        sum += segment[x][tmp];
    return sum;
  }
 public:
  FenwickTree(const vector<vector<V>>& data) {
    size_t nx = data.size(), ny = data[0].size();
    segment.assign(nx + 1, {});
    for (size_t i = 0; i <= nx; ++i) {
      segment[i].assign(ny + 1, 0);
      for (size_t j = 1; j <= ny; j++) update(data[i-1][j-1], i, j);
    }
  }
  void update(V val, ii x, ii y) {  // Update x x y with val
    update(val, x.second, y.second); update(val, x.first - 1, y.first - 1);
    update(-val, x.second, y.first - 1); update(-val, x.first - 1, y.second);
  }
  int query(ii x, ii y) {  // Get range sum of x x y
    return query(x.second, y.second) - query(x.first - 1, y.second)
        - query(x.second, y.first - 1) + query(x.first - 1, y.first - 1);
  }
};
\end{minted}
\end{listing}
\subsection{Segment Tree}
\label{sec:segment-tree}
\begin{listing}[H]
\caption{Lazy segment tree for 1D range sum query}
\begin{minted}{c++}
template <typename V = int>
class SegmentTree {
 private:
  size_t size; vector<V> segment, lazy;
  size_t left(size_t p) { return p << 1; }
  size_t right(size_t p) { return (p << 1) + 1; }
  void build(const vector<V>& data, size_t p, size_t L, size_t R) { // O(n)
    if (L == R) segment[p] = data[L];
    else {
      build(data, left(p), L, (L + R) / 2);
      build(data, right(p), (L + R) / 2 + 1, R);
      segment[p] = segment[left(p)] + segment[right(p)];
    }
  }
  void updateLazy(V val, size_t p, size_t L, size_t R) {
    segment[p] += lazy[p] * (R-L+1);
    if (L != R) lazy[left(p)] = lazy[right(p)] = lazy[p];
  }
  void update(V val, size_t p, size_t L, size_t R,
              size_t i, size_t j) { // O(log n)
    if (lazy[p] != 0) updateLazy(lazy[p], p, L, R), lazy[p] = 0;
    if (L > R || L > j || R < i) return;
    if (L >= i && R <= j) updateLazy(val, p, L, R);
    else {
      update(val, left(p), L, (L + R) / 2, i, j);
      update(val, right(p), (L + R) / 2 + 1, R, i, j);
      segment[p] = segment[left(p)] + segment[right(p)];
    }
  }
  V query(size_t p, size_t L, size_t R, size_t i, size_t j) { // O(log n)
    if (lazy[p] != 0) updateLazy(lazy[p], p, L, R), lazy[p] = 0;
    if (L > R || L > j || R < i) return 0;
    if (L >= i && R <= j) return segment[p];
    return query(left(p), L, (L + R) / 2, i, j) +
        query(right(p), (L + R) / 2 + 1, R, i, j);
  }
 public:
  explicit SegmentTree(const vector<V>& data) {
    size = data.size(); segment.assign(4*size, 0); lazy.assign(4*size, 0);
    build(data, 1, 1, size);
  }
  void update(V val, size_t i, size_t j) {  // Update [i, j] with val
    update(val, 1, 1, size, i, j);
  }
  V query(size_t i, size_t j) {  // Get range query of [i, j]
    return query(1, 1, size, i, j);
  }
};
\end{minted}
\end{listing}
\subsection{Disjoint Set}
\label{sec:disjoint-set}
\begin{listing}[H]
\caption{Disjoint set}
\begin{minted}{c++}
template<class K = int, class Compare = less<K>>
class DisjointSet {
 private:
  using V = struct { K parent; size_t rank; size_t size; };
  map<K, V, Compare> mem; size_t n_partition;
  const K& root(const K& k) {
    return (mem[k].parent == k) ? k : (mem[k].parent = root(mem[k].parent));
  }
 public:
  DisjointSet() { mem.clear(); n_partition = 0; }
  void addElem(const K& k) {
    if (mem.count(k) == 0) mem[k] = {k, 0, 1}, ++n_partition;
  }
  void reset() {
    for (V& c : mem) c.second = {c.first, 0, 1}; n_partition = mem.size();
  }
  bool isSame(const K& kl, const K& kr) {
    return root(kl) == root(kr);
  }
  void makeUnion(const K& kl, const K& kr) {
    K& l = root(kl), r = root(kr);
    if (l != r) {
      --n_partition;
      if (mem[l].rank > mem[r].rank) swap(l, r);
      else if (mem[l].rank == mem[r].rank) ++mem[r].rank;
      mem[l].parent = r;
      mem[r].size += mem[l].size;
    }
  }
  size_t numPartition() { return n_partition; }
  size_t sizeOfSet(const K& k) { return mem[root(k)].size; }
};
\end{minted}
\end{listing}
\section{Graphs}
\label{sec:graph-algorithms}
\subsection{Traversal Based On DFS/BFS}
\label{sec:search-algorithms}
\begin{listing}[H]
\caption{Topologitcal sort in directed acyclic graph}
\begin{minted}{c++}
using V = int;
map<V, vector<V>> outgoings, incomings; vector<V> topoSorted;
void topoSortDFSAux(map<V, bool>& visited, const V& u) {
  visited[u] = true;
  TRAV(v, outgoings[u]) if (!visited[v]) topoSortDFSAux(visited, v);
  topoSorted.push_back(u);
}
void topoSortDFS() {
  topoSorted.clear();
  map<V, bool> visited; TRAV(u, outgoings) visited[u.first] = false;
  TRAV(u, outgoings)
    if (!visited[u.first]) topoSortDFSAux(visited, u.first);
  reverse(ALL(topoSorted));
}
void topoSortBFS() {
  topoSorted.clear();
  queue<V> q; TRAV(u, incomings) if (u.second.size() == 0) q.push(u.first);
  while (!q.empty()) {
    V u = q.front(); q.pop(); topoSorted.push_back(u); incomings.erase(u);
    TRAV(v, incomings) {
      v.second.erase(find(ALL(v.second), u));
      if (v.second.size() == 0) q.push(v.first);
    }
  }
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Edges property check in DFS spanning tree}
\begin{minted}{c++}
using V = int;
enum STAT_E { TREE, BACK_PARENT, BACK_CYCLE, FORWARD };
enum STAT_V { UNVISITED, EXPLORING, VISITED };
map<V, vector<V>>& outgoings; map<pair<V, V>, STAT_E> edgeProp;
void setEdgePropAux(map<V, STAT_V>& status, map<V, V>& parent, const V& u) {
  status[u] = EXPLORING;
  TRAV(v, outgoings[u]) {
    auto key = make_pair(u, v);
    switch(status[v]) {
      case UNVISITED:
        parent[v] = u, edgePropAux(status, parent, v); edgeProp[key] = TREE;
        break;
      case EXPLORING:
        edgeProp[key] = (v == parent[u]) ? BACK_PARENT : BACK_CYCLE;
        break;
      case VISITED:
        edgeProp[key] = FORWARD;
        break;
    }
  }
  status[u] = VISITED;
}
void setEdgeProp() {
  edgeProp.clear(); map<V, STAT_V> status; map<V, V> parent;
  TRAV(u, outgoings) status[u.first] = UNVISITED, parent[u.first] = u.first;
  TRAV(u, outgoings)
    if (status[u.first] == UNVISITED) edgePropAux(status, parent, u.first);
}
STAT_E getEdgeProp(const V& u, const V& v) { return edgeProp[{u, v}]; }
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Articulation points and bridges in undirected graph}
\begin{minted}{c++}
using V = int;
map<V, vector<V>> outgoings;
map<V, bool> articulationPoints; vector<pair<V, V>> bridges;
void setCriticalsAux(map<V, int>& num, map<V, int>& low, map<V, V>& parent,
                     V& root, int& counter, int& children, const V& u) {
  num[u] = low[u] = ++counter;
  for (const auto& v : outgoings[u]) {
    if (num[v] < 0) {
      if (u == root) ++children; parent[v] = u;
      setCriticalsAux(num, low, parent, root, counter, children, v);
      if (low[v] >= num[u]) articulationPoints[u] = true;
      if (low[v] > num[u]) bridges.push_back({u, v});
      low[u] = min(low[u], low[v]);
    } else if (v != parent[u])
      low[u] = min(low[u], num[v]);
  }
}
void setCriticals() {
  articulationPoints.clear(); bridges.clear();
  map<V, int> num, low; map<V, V> parent; int counter = 0;
  for (const auto& c : outgoings) {
    num[c.first] = -1; low[c.first] = 0;
    parent[c.first] = c.first; articulationPoints[c.first] = false;
  }
  for (const auto& c : outgoings) {
    if (num[c.first] < 0) {
      V root = c.first; int children = 0;
      setCriticalsAux(num, low, parent, root, counter, children, c.first);
      articulationPoints[root] = children > 1;
    }
  }
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Strongly connected components in directed graph}
\begin{minted}{c++}
using V = int;
map<V, vector<V>> outgoings; vector<vector<V>> SCC;
void setSCCAux(map<V, int>& num, map<V, int>& low, map<V, bool> visited,
               stack<V>& s, int& counter, const V& u) {
  num[u] = low[u] = ++counter; visited[u] = true; s.push(u);
  for (const auto& v : outgoings[u]) {
    if (num[v] < 0) setSCCAux(num, low, visited, s, counter, v);
    if (visited[v]) low[u] = min(low[u], low[v]);
  }
  if (low[u] == num[u]) {
    vector<V> tmp;
    while (true) {
      auto v = s.top(); s.pop(); visited[v] = false; tmp.push_back(v);
      if (u == v) break;
    }
    SCC.push_back(tmp);
  }
}
void setSCC() {
  SCC.clear(); map<V, int> num, low; map<V, bool> visited;
  stack<V> s; int counter = 0;
  num.clear(); low.clear(); visited.clear();
  for (const auto& c : outgoings)
    num[c.first] = -1, low[c.first] = 0, visited[c.first] = false;
  for (const auto& c : outgoings)
    if (num[c.first] < 0) setSCCAux(num, low, visited, s, counter, c.first);
}
\end{minted}
\end{listing}
\subsection{Minimum Spanning Tree}
\label{sec:minim-spann-tree}
\begin{listing}[H]
\caption{Prim algorithm}
\begin{minted}{c++}
using V = int; using W = int; using E = pair<W, pair<V, V>>;
map<V, vector<pair<V, W>>> outgoings; vector<E> MST;
void setMST() {
  MST.clear(); priority_queue<E, vector<E>, greater<E>> pq;
  map<V, bool> taken; TRAV(c, outgoings) taken[c.first] = false;
  TRAV(u, outgoings) {
    if (taken[u.first]) continue; taken[u.first] = true;
    TRAV(v, outgoings[u.first])
      if (!taken[v.first]) pq.push({v.second, {v.first, u.first}});
    while (!pq.empty()) {
      auto w = pq.top().first; auto tmp = pq.top().second; pq.pop();
      if (taken[tmp.first]) continue; taken[tmp.first] = true;
      TRAV(v, outgoings[tmp.first]) {
        if (!taken[v.first]) {
          pq.push({v.second, {v.first, tmp.first}});
          MST.push_back({w, {v.first, tmp.first}});
        }
      }
    }
  }
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Kruskal algorithm with suboptimal MST}
\begin{minted}{c++}
using V = int; using W = int; using E = pair<W, pair<V, V>>;
vector<V> vertices; vector<E> edges, MST, subMST;
void setMST() {
  MST.clear(); sort(ALL(edges));
  DisjointSet<V> parti; TRAV(v, vertices) parti.addElem(v);
  TRAV(e, edges) {
    if (!parti.isSame(e.second.first, e.second.second)) {
      MST.push_back(e);
      parti.makeUnion(e.second.first, e.second.second);
    }
  }
}
void setSubMST() {
  subMST.clear(); W subopt = oo;
  DisjointSet<V> parti; TRAV(v, vertices) parti.addElem(v);
  TRAV(exclude, MST) {
    vector<E> tmptree; W tmp = 0; parti.reset();
    TRAV(e, edges) {
      if (e == exclude) continue;
      if (!parti.isSame(e.second.first, e.second.second)) {
        tmptree.push_back(e); tmp += e.first;
        parti.makeUnion(e.second.first, e.second.second);
      }
    }
    if (tmp < subopt) subopt = tmp, subMST = tmptree;
  }
}
\end{minted}
\end{listing}
\subsection{Shortest Paths}
\label{sec:shortest-paths}
\begin{listing}[H]
\caption{Dijkstra algorithm}
\begin{minted}{c++}
using V = int; using W = int;
map<V, vector<pair<V, W>>> outgoings; vector<V> vertices;
W findShortest(const V& s, const V& t) {
  map<V, W> dist; TRAV(c, vertices) dist[c] = oo;
  using E = pair<W, V>; priority_queue<E, vector<E>, greater<E>> pq;
  pq.push({dist[s] = 0, s});
  while (!pq.empty()) {
    W d = pq.top().first; V u = pq.top().second; pq.pop();
    if (d > dist[u]) continue;
    if (u == t) break;  // should be removed if negative edge exists
    TRAV(v, outgoings[u])
      if (dist[u] + v.second < dist[v.first])
        pq.push({dist[v.first] = dist[u] + v.second, v.first});
  }
  return dist[t];
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Bellman--Ford algorithm}
\begin{minted}{c++}
using V = int; using W = int;
map<V, vector<pair<V, W>>> outgoings; vector<V> vertices;
W findShortest(const V& s, const V& t) {
  map<V, W> dist; TRAV(c, vertices) dist[c] = oo;
  REP(i, 1, SZ(vertices)) TRAV(u, vertices) TRAV(v, outgoings[u])
    dist[v.first] = min(dist[v.first], dist[u] + v.second);
  TRAV(u, vertices) TRAV(v, outgoings[u])
    if (dist[v.first] > dist[u] + v.second)
      throw domain_error("Negative cycle");
  return dist[t];
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{Floyd--Warshall algorithm}
\begin{minted}{c++}
using V = int; using W = int;
map<V, vector<pair<V, W>>> outgoings; vector<V> vertices;
map<V, map<V, W>> shortests; map<V, map<V, V>> parent;
W setShortests() {
  shortests.clear(); TRAV(i, vertices) TRAV(j, vertices) shortests[i][j] = oo;
  TRAV(u, outgoings) TRAV(v, u.second) shortests[u.first][v.first] = v.second;
  parent.clear(); TRAV(i, vertices) TRAV(j, vertices) parent[i][j] = i;
  TRAV(k, vertices) TRAV(i, vertices) TRAV(j, vertices)
    if (shortests[i][j] > shortests[i][k] + shortests[k][j]) {
      shortests[i][j] = shortests[i][k] + shortests[k][j];
      parent[i][j] = parent[k][j];
    }
  TRAV(i, vertices) {
    // shortests[i][i]: weight of cheapest cycle starting from i
    if (shortests[i][i] < 0) throw domain_error("Negative cycle");
    else if (shortests[i][i] < oo) continue;
  }
}
void printPath(const V& s, const V& t) {
  if (s != t) printPath(s, parent[s][t]); cout << t;
}
\end{minted}
\end{listing}

\subsection{Maximum Flow}
\label{sec:maximum-flow}
\begin{listing}[H]
\caption{Dinic algorithm}
\begin{minted}{c++}
using V = int; using W = int;
map<V, map<V, W>> capacity; vector<V> vertices; map<V, map<V, W>> residual;
map<V, map<V, W>::iterator> iter; map<V, int> level;
bool bfs(const V& s, const V& t) {
  TRAV(u, vertices) level[u] = -1; level[s] = 0; queue<V> q({s});
  while(!q.empty()) {
    V u = q.front(); q.pop(); if (u == t) break;
    TRAV(v, residual[u]) if (v.second > 0 && level[v.first] < 0)
      level[v.first] = level[u] + 1, q.emplace(v.first);
  }
  return level[t] >= 0;
}
W augment(const V& u, const V& t, W flow) {
  if (u == t || flow == 0) return flow;
  for (; iter[u] != residual[u].end(); ++iter[u])
    if (level[iter[u]->first] == level[u] + 1)
      if (W f = augment(iter[u]->first, t, min(flow, iter[u]->second))) {
        iter[u]->second -= f, residual[iter[u]->first][u] += f;
        return f;
      }
  return 0;
}
W findMaxFlow(const V& s, const V& t) {
  residual.clear(); level.clear(); iter.clear();
  TRAV(u, capacity) TRAV(v, u.second) residual[v.first][u.first] = 0;
  TRAV(u, capacity) TRAV(v, u.second) residual[u.first][v.first] += v.second;
  W maxflow = 0;
  while (bfs(s, t)) {
    TRAV(u, vertices) iter[u] = residual[u].begin();
    while (W flow = augment(s, t, oo)) maxflow += flow;
  }
  return maxflow;
}
\end{minted}
\end{listing}
\begin{listing}[H]
\caption{FIFO push-relabel algorithms with gap heuristic}
\begin{minted}{c++}
using V = int; using W = ll;
map<V, map<V, W>> capacity; vector<V> vertices;
map<V, map<V, W>> residual; map<V, W> excess; map<V, int> dist;
map<V, bool> active; vector<int> label; queue<V> q;
void enqueue(const V& u) {
  if (!active[u] && excess[u] > 0) active[u] = true, q.push(u);
}
void gap(int k) {
  TRAV(u, vertices) {
    if (dist[u] < k) continue;
    --label[dist[u]];
    dist[u] = max(dist[u], 1 + SZ(vertices));
    ++label[dist[u]];
    enqueue(u);
  }
}
void push(const V& u, const V& v) {
  W d = min(excess[u], residual[u][v]);
  if (d == 0 || dist[u] <= dist[v]) return;
  residual[u][v] -= d, residual[v][u] += d;
  excess[u] -= d, excess[v] += d;
  enqueue(v);
}
void relabel(const V& u) {
  --label[dist[u]];
  dist[u] = 2 * SZ(vertices);
  TRAV(v, residual[u])
    if (v.second > 0) dist[u] = min(dist[u], dist[v.first] + 1);
  ++label[dist[u]];
  enqueue(u);
}
void discharge(const V& u) {
  TRAV(v, residual[u]) {
    if (excess[u] <= 0) break; push(u, v.first);
  }
  if (excess[u] > 0) label[dist[u]] == 1 ? gap(dist[u]) : relabel(u);
}
W findMaxFlow(const V& s, const V& t) {
  residual.clear();
  TRAV(u, capacity) TRAV(v, u.second) residual[v.first][u.first] = 0;
  TRAV(u, capacity) TRAV(v, u.second) residual[u.first][v.first] += v.second;
  excess.clear(); dist.clear(); active.clear(); label.clear();
  dist[s] = SZ(vertices); active[s] = active[t] = true;
  label.assign(2 * SZ(vertices), 0);
  label[0] = SZ(vertices) - 1; label[SZ(vertices)] = 1;
  while (!q.empty()) q.pop();
  TRAV(v, capacity[s]) excess[s] += v.second;
  W maxflow = excess[s];
  TRAV(v, capacity[s]) push(s, v.first);
  while (!q.empty()) {
    V u = q.front(); q.pop(); active[u] = false; discharge(u);
  }
  TRAV(v, residual[s]) maxflow -= v.second;
  return maxflow;
}
\end{minted}
\end{listing}
\end{document}
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
