#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cout << "Case #" << i << ":\n";
    string s = "", s_tmp; int V = 0, V_tmp;
    for (auto j = 0; j < 10; ++j) {
      cin >> s_tmp >> V_tmp;
      if (V_tmp > V) {
        s = s_tmp + '\n'; V = V_tmp;
      }
      else if (V_tmp == V) s += s_tmp + '\n';
    }
    cout << s;
  }
}
