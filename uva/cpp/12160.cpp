#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int dist[10000];

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  auto T = 0;
  while (++T) {
    int L, U, R; cin >> L >> U >> R; if (L == 0 && U == 0 && R == 0) break;
    vector<int> dist, buttons; dist.assign(10000, -1); buttons.assign(R, 0);
    REP(i, 0, R) cin >> buttons[i];
    queue<int> q; q.push(L); dist[L] = 0; bool found = L == U;
    while (!found && !q.empty()) {
      auto c = q.front(); q.pop();
      TRAV(b, buttons) {
        auto tmp = (c + b)%10000;
        if (dist[tmp] < 0) q.push(tmp), dist[tmp] = dist[c] + 1;
        if (tmp == U) { found = true; break; }
      }
    }
    cout << "Case " << T << ": ";
    if (dist[U] < 0) cout << "Permanently Locked\n";
    else cout << dist[U] << '\n';
  }
}
