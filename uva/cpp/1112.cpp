#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int n_case; cin >> n_case; while (n_case--) {
    int N, E, T, M; cin >> N >> E >> T >> M; --E;
    vector<vii> graph; graph.assign(N, {});
    while (M--) {
      int a, b, t; cin >> a >> b >> t; graph[b-1].push_back({t, a-1});
    }
    vi dist; dist.assign(N, oo >> 1); dist[E] = 0;
    priority_queue<ii, vii, greater<ii>> pq; pq.push({dist[E], E});
    while (!pq.empty()) {
      auto w = pq.top().first, u = pq.top().second; pq.pop();
      if (w > dist[u]) continue;
      for (const auto& v : graph[u]) {
        if (dist[u] + v.first < dist[v.second]) {
          dist[v.second] = dist[u] + v.first;
          pq.push({dist[v.second], v.second});
        }
      }
    }
    auto result = 0; for (const auto& d : dist) if (d <= T) ++result;
    cout << result << '\n'; if (n_case) cout << '\n';
  }
}
