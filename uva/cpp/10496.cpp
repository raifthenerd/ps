#include <bits/stdc++.h>

using namespace std;

int n, adj[11][11], cache[11][2048];
int solve(int curr, unsigned int mask) {
  if (mask == (1U << n) - 1) return adj[curr][0];
  if (cache[curr][mask] <= 0) {
    cache[curr][mask] = numeric_limits<int>::max();
    for (auto nxt = 0; nxt < n; ++nxt) {
      if (curr == nxt) continue;
      if (mask & (1U << nxt)) continue;
      cache[curr][mask] = min(cache[curr][mask],
                              adj[curr][nxt] + solve(nxt, mask | (1U << nxt)));
    }
  }
  return cache[curr][mask];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int w, h, x, y, nb; cin >> w >> h >> x >> y >> nb;
    vector<pair<int, int>> beeper {{x, y}};
    while (nb--) {
      cin >> x >> y; beeper.push_back({x, y});
    }
    n = beeper.size();
    for (auto i = 0; i < n; ++i) {
      adj[i][i] = 0;
      for (auto j = i + 1; j < n; ++j) {
        auto tmp = abs(beeper[i].first - beeper[j].first);
        tmp += abs(beeper[i].second - beeper[j].second);
        adj[i][j] = adj[j][i] = tmp;
      }
    }
    for (auto i = 0; i < 11; ++i)
      for (auto j = 0; j < 2048; ++j)
        cache[i][j] = 0;
    cout << "The shortest path has length " << solve(0, 1) << '\n';
  }
}
