#include <bits/stdc++.h>

using namespace std;

int n, m;
vector<long long> cumsums;

bool valid(long long query) {
  auto it = upper_bound(cumsums.begin(), cumsums.end(), query);
  for (auto i = 2; i <= m; ++i)
    it = upper_bound(it, cumsums.end(), *prev(it) + query);
  return it == cumsums.end();
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (cin >> n) {
    cin >> m;
    long long base = 0, sum = 0;
    cumsums.clear();
    for (auto i = 0; i < n; ++i) {
      long long c; cin >> c;
      base = max(base, c); sum += c; cumsums.push_back(sum);
    }
    auto result = base;
    while (!valid(result)) result <<= 1;
    if (result != base) {
      auto lb = result >> 1;
      while (result - lb != 1) {
        auto q = (result + lb) / 2;
        if (valid(q)) result = q;
        else lb = q;
      }
    }
    cout << result << '\n';
  }
}
