#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

map<string, set<string>> graph;
map<string, int> dfs_num, dfs_low;
map<string, bool> visited;

stack<string> S;
vector<vector<string>> circles;
int counter;

void SCC(string node) {
  S.push(node); visited[node] = true;
  dfs_num[node] = dfs_low[node] = ++counter;
  for (const auto& c : graph[node]) {
    if (dfs_num[c] == oo) SCC(c);
    if (visited[c]) dfs_low[node] = min(dfs_low[node], dfs_low[c]);
  }
  if (dfs_low[node] == dfs_num[node]) {
    bool first = true;
    while (true) {
      auto v = S.top(); S.pop(); visited[v] = false;
      if (first) first = false; else cout << ", "; cout << v;
      if (node == v) break;
    }
    cout << '\n';
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0;
  while (true) {
    int n, m; cin >> n >> m; if (n == 0 && m == 0) break;
    graph.clear(); dfs_num.clear(); dfs_low.clear(); visited.clear();
    while (m--) {
      string x, y; cin >> x >> y; graph[x].insert(y);
      dfs_num[x] = dfs_num[y] = oo;
      dfs_low[x] = dfs_low[y] = 0;
      visited[x] = visited[y] = false;
    }
    if (T) cout << '\n';
    cout << "Calling circles for data set " << ++T << ":\n";
    while(!S.empty()) S.pop(); circles.clear(); counter = 0;
    for (const auto& kv : graph) if (dfs_num[kv.first] == oo) SCC(kv.first);
  }
}
