#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N; cin >> N;
  while (N--) {
    int A, B, C; cin >> A >> B >> C;
    bool found = false;
    for (auto x = -100; !found && x <= 100; ++x) {
      for (auto y = x+1; !found && y <= 100; ++y) {
        for (auto z = y+1; !found && z <= 100; ++z) {
          if (x + y + z == A &&
              x * y * z == B &&
              x * x + y * y + z * z == C) {
            cout << x << ' ' << y << ' ' << z << '\n';
            found = true;
          }
        }
      }
    }
    if (!found) cout << "No solution.\n";
  }
}
