#include <bits/stdc++.h>

using namespace std;

class Partition {
 private:
  int H, W;
  int image[200][200], visited[200][200];
  int rank[201][201];
  pair<int, int> root[201][201];
  const list<pair<int, int>> diffs {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
  pair<int, int> bound {200, 200};
  pair<int, int>& representative(pair<int, int>& x) {
    return (x == root[x.first][x.second]) ?
        x : (root[x.first][x.second] = representative(root[x.first][x.second]));
  }
 public:
  Partition(int H, int W) {
    this->H = H; this->W = W;
    for (auto y = 0; y < H; ++y) { char tmp[W >> 2]; cin >> tmp;
      for (auto x = 0; x < (W >> 2); ++x) {
        if (tmp[x] >= '0' && tmp[x] <= '9') tmp[x] -= '0';
        else tmp[x] -= 'a' - 10;
        for (auto j = 0; j < 4; ++j) {
          image[y][4*x+j] = (tmp[x] & (1 << (3-j))) > 0;
        }
      }
    }
  }
  bool sameSet(pair<int, int>& x, pair<int, int>& y) {
    return representative(x) == representative(y);
  }
  void uniteSets(pair<int, int>& x, pair<int, int>& y) {
    x = representative(x); y = representative(y);
    if (x != y) {
      if (rank[x.first][x.second] > rank[y.first][y.second]) swap(x, y);
      if (rank[x.first][x.second] == rank[y.first][y.second]) ++rank[y.first][y.second];
      root[x.first][x.second] = y;
    }
  }
  string solve() {
    queue<pair<int, int>> q;
    for (auto j = 0; j < H; ++j) {
      for (auto i = 0; i < W; ++i) {
        visited[j][i] = false; rank[j][i] = 0; root[j][i] = {j, i};
      }
    }
    rank[200][200] = 0; root[200][200] = {200, 200};
    for (auto j = 0; j < H; ++j) {
      for (auto i = 0; i < W; ++i) {
        if (visited[j][i]) continue;
        auto key = make_pair(j, i);
        q.push(key); visited[j][i] = true;
        while (!q.empty()) {
          auto tmp = q.front(); auto y = tmp.first, x = tmp.second; q.pop();
          uniteSets(key, tmp);
          if (x*y == 0 || y == H-1 || x == W-1) uniteSets(bound, tmp);
          for (auto& d : diffs) {
            if (0 <= y + d.first && y + d.first < H &&
                0 <= x + d.second && x + d.second < W &&
                image[y][x] == image[y + d.first][x + d.second] &&
                !visited[y + d.first][x + d.second]) {
              q.push({y + d.first, x + d.second});
              visited[y + d.first][x + d.second] = true; }}}}}
    for (auto j = 0; j < H; ++j)
      for (auto i = 0; i < W; ++i)
        visited[j][i] = false;
    string result;
    vector<pair<int, int>> nbd;
    for (auto j = 0; j < H; ++j) {
      for (auto i = 0; i < W; ++i) {
        if (!image[j][i] || visited[j][i]) continue;
        nbd.clear();
        auto key = make_pair(j, i);
        q.push(key); visited[j][i] = true;
        while (!q.empty()) {
          auto tmp = q.front(); auto y = tmp.first, x = tmp.second; q.pop();
          for (auto& d : diffs) {
            tmp = make_pair(y + d.first, x + d.second);
            if (0 <= y + d.first && y + d.first < H &&
                0 <= x + d.second && x + d.second < W) {
              if (image[y][x] != image[y + d.first][x + d.second]) {
                bool add = true;
                for (auto& c : nbd) add = add && !sameSet(c, tmp);
                if (add) nbd.push_back(tmp);
              } else if (!visited[y + d.first][x + d.second]) {
                q.push(tmp); visited[y + d.first][x + d.second] = true; }}}}
        switch (nbd.size()) {
          case 1: result.push_back('W'); break;
          case 2: result.push_back('A'); break;
          case 3: result.push_back('K'); break;
          case 4: result.push_back('J'); break;
          case 5: result.push_back('S'); break;
          case 6: result.push_back('D'); break;
        }
    }}
    sort(result.begin(), result.end());
    return result;
  }
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (true) {
    int H, W; cin >> H >> W; if (H == 0 && W == 0) break; W <<= 2;
    Partition P(H, W);
    cout << "Case " << ++T << ": " << P.solve() << '\n';
  }
}
