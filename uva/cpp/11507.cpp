#include <bits/stdc++.h>

using namespace std;

const char states[6][3] = {"+y", "-y", "+z", "-z", "+x", "-x"};
const int fsm[6][4] = {{5, 4, 0, 0},
                       {4, 5, 1, 1},
                       {2, 2, 5, 4},
                       {3, 3, 4, 5},
                       {0, 1, 2, 3},
                       {1, 0, 3, 2}};
map<string, int> statemap = {{"+y", 0},
                             {"-y", 1},
                             {"+z", 2},
                             {"-z", 3},
                             {"+x", 4},
                             {"-x", 5}};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int L;
  while (true) {
    cin >> L;
    if (L == 0) break;
    queue<string> decisions; string s;
    int state = 4;
    while (--L) {
      cin >> s; decisions.push(s);
    }
    while (!decisions.empty()) {
      if (decisions.front() != "No")
        state = fsm[state][statemap[decisions.front()]];
      decisions.pop();
    }
    cout << states[state] << '\n';
  }
}
