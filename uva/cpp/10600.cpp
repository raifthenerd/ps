#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

class DisjointSet {
 private:
  int size, n_set; vi rank, parent;
  int representative(int i) {
    return (i == parent[i]) ? i : (parent[i] = representative(parent[i]));
  }
 public:
  explicit DisjointSet(int _size) {
    size = _size; reset();
  }
  void reset() {
    n_set = size;
    rank.clear(); rank.assign(size, 0);
    parent.clear(); parent.assign(size, 0);
    for (auto i = 0; i < size; ++i) parent[i] = i;
  }
  void unionSet(int i, int j) {
    i = representative(i), j = representative(j);
    if (i != j) {
      if (rank[i] > rank[j]) swap(i, j);
      else if (rank[i] == rank[j]) ++rank[j];
      parent[i] = j; --n_set;
    }
  }
  bool isSameSet(int i, int j) {
    return representative(i) == representative(j);
  }
  int numSet() { return n_set; }
};


int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T;
  while (T--) {
    int N, M; cin >> N >> M;
    vector<pair<int, ii>> edges; vi exclude; DisjointSet parti(N);
    while (M--) {
      int A, B, C; cin >> A >> B >> C; edges.push_back({C, {A-1, B-1}});
    }
    sort(ALL(edges));
    auto optimal = 0;
    for (auto i = 0; i < SZ(edges); ++i) {
      auto u = edges[i].second.first, v = edges[i].second.second;
      if (!parti.isSameSet(u, v)) {
        exclude.push_back(i);
        optimal += edges[i].first;
        parti.unionSet(u, v);
      }
    }
    auto suboptimal = oo;
    for (auto e : exclude) {
      parti.reset();
      auto tmp = 0;
      for (auto i = 0; i < SZ(edges); ++i) {
        if (i == e) continue;
        auto u = edges[i].second.first, v = edges[i].second.second;
        if (!parti.isSameSet(u, v)) {
          tmp += edges[i].first;
          parti.unionSet(u, v);
        }
      }
      if (parti.numSet() == 1) suboptimal = min(suboptimal, tmp);
    }
    cout << optimal << ' ' << suboptimal << '\n';
  }
}
