#include <bits/stdc++.h>

using namespace std;

const char matrix[9][9] = {{'O', 'B', 'I', 'D', 'A', 'I', 'B', 'K', 'R'},
                           {'R', 'K', 'A', 'U', 'L', 'H', 'I', 'S', 'P'},
                           {'S', 'A', 'D', 'I', 'Y', 'A', 'N', 'N', 'O'},
                           {'H', 'E', 'I', 'S', 'A', 'W', 'H', 'I', 'A'},
                           {'I', 'R', 'A', 'K', 'I', 'B', 'U', 'L', 'S'},
                           {'M', 'F', 'B', 'I', 'N', 'T', 'R', 'N', 'O'},
                           {'U', 'T', 'O', 'Y', 'Z', 'I', 'F', 'A', 'H'},
                           {'L', 'E', 'B', 'S', 'Y', 'N', 'U', 'N', 'E'},
                           {'E', 'M', 'O', 'T', 'I', 'O', 'N', 'A', 'L'}};

string names[8] = {"RAKIBUL",
                   "ANINDYA",
                   "MOSHIUR",  // M
                   "SHIPLU",   // P
                   "KABIR",
                   "SUNNY",
                   "OBAIDA",
                   "WASI"};    // W

// Note that all names except SUNNY have exactly one 'I'.
// Also, all names with 'I' can be partitioned with 'A' and 'H'.

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << "KABIR\n";
}
