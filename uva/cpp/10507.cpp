#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t N, M;
  while (cin >> N) {
    cin >> M;
    set<char> awake;
    for (auto i = 0; i < 3; ++i) {
      char x; cin >> x;
      awake.insert(x);
    }
    map<char, set<char>> graph;
    while (M--) {
      char x, y; cin >> x >> y;
      graph[x].insert(y); graph[y].insert(x);
    }

    if (N == 3) {
      cout << "WAKE UP IN, 0, YEARS\n";
      continue;
    }
    if (graph.size() != N) {
      cout << "THIS BRAIN NEVER WAKES UP\n";
      continue;
    }

    size_t prev, year = 0;
    do {
      ++year; prev = awake.size();
      map<char, size_t> tmp;
      for (auto node : awake)
        for (auto nbd : graph[node]) ++tmp[nbd];
      for (auto node : tmp)
        if (node.second >= 3) awake.insert(node.first);
    } while (prev != awake.size() && awake.size() != N);
    if (awake.size() != N) cout << "THIS BRAIN NEVER WAKES UP\n";
    else cout << "WAKE UP IN, " << year << ", YEARS\n";
  }
}
