#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int H, U, D, F; cin >> H >> U >> D >> F;
    int day = 0; double curr = 0.0, climb = U, fatigue = U * F / 100.0;
    bool success = false;
    if (H == 0) break;
    while (++day) {
      curr += climb > 0.0 ? climb : 0.0;
      if (curr > H) {
        success = true;
        break;
      }
      curr -= D; climb -= fatigue;
      if (curr < 0.0) {
        success = false;
        break;
      }
    }
    cout << (success ? "success" : "failure") << " on day " << day << '\n';
  }
}
