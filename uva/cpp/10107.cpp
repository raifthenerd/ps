#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  vector<unsigned int> v;
  unsigned int n, N = 0;
  while (cin >> n) {
    v.insert(upper_bound(v.begin(), v.end(), n), n);
    unsigned int tmp = ++N >> 1;
    cout << (N & 1 ? v[tmp] : (v[tmp] + v[tmp - 1]) / 2 ) << '\n';
  }
}
