#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N; cin >> N; if (N == 0) break;
    priority_queue<int, vector<int>, greater<int>> xs;
    for (auto n = 0; n < N; ++n) {
      int x; cin >> x; xs.push(x);
    }
    long long total = 0;
    while (xs.size() > 1) {
      auto cost = xs.top(); xs.pop();
      cost += xs.top(); xs.pop();
      xs.push(cost);
      total += cost;
    }
    cout << total << '\n';
  }
}
