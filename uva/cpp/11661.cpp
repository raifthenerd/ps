#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int L; cin >> L;
    if (L == 0) break;
    char prev = '.', curr; int result = L, j;
    for (auto i = 0; i < L; ++i) {
      cin >> curr;
      if (curr == '.') continue;
      else if (curr == 'Z') result = 0;
      else {
        if (prev != '.' && prev != curr) result = min(result, i-j);
        prev = curr;
        j = i;
      }
    }
    cout << result << '\n';
  }
}
