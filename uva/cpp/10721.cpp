#include <bits/stdc++.h>

using namespace std;

long long mem[51][51][51];

long long BC(int n, int k, int m) {
  if (k > n || m*k < n) return 0;
  if (k == 1 || n == k || m*k == n) return 1;
  if (mem[n][k][m] < 0) {
    mem[n][k][m] = 0;
    for (auto l = 1; l <= m; ++l) mem[n][k][m] += BC(n-l, k-1, m);
  }
  return mem[n][k][m];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n, k, m;
  for (auto i = 0; i <= 50; ++i)
    for (auto j = 0; j <= 50; ++j)
      for (auto k = 0; k <= 50; ++k)
        mem[i][j][k] = -1;
  while (cin >> n) { cin >> k >> m;
    cout << BC(n, k, m) << '\n';
  }
}
