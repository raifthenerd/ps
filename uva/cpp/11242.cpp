#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(2);
  while (true) {
    int f, r; cin >> f; if (f == 0) break; cin >> r;
    vector<int> front, rear;
    while (f--) { int x; cin >> x; front.push_back(x); }
    while (r--) { int x; cin >> x; rear.push_back(x); }
    set<double> drives;
    for (auto f : front) {
      for (auto r : rear) {
        drives.insert(static_cast<double>(r) / f);
      }
    }
    auto pre = drives.begin(), curr = next(pre);
    double result = *curr / *pre;
    while (curr != drives.end()) {
      double tmp = *curr / *pre;
      if (tmp > result) result = tmp;
      ++curr; ++pre;
    }
    cout << result << '\n';
  }
}
