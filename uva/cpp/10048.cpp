#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int C; vector<map<int, int>> graph; vector<bool> taken;
priority_queue<pair<int, ii>> pq;

void process(int u) {
  taken[u] = true;
  for (const auto& c : graph[u])
    if (!taken[c.first]) pq.push({-c.second, {u, c.first}});
}

int dfs(int u, int target) {
  taken[u] = true;
  if (u == target) return 0;
  for (const auto& c : graph[u]) {
    if (taken[c.first]) continue;
    auto tmp = dfs(c.first, target);
    if (tmp >= 0) return max(tmp, graph[u][c.first]);
  }
  return -1;
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0;
  while (true) {
    int S, Q; cin >> C >> S >> Q; if (C == 0 && S == 0 && Q == 0) break;
    if (T) cout << '\n'; cout << "Case #" << ++T << '\n';
    graph.clear(); graph.assign(C, {});
    while (S--) {
      int c1, c2, d; cin >> c1 >> c2 >> d; --c1; --c2;
      graph[c1][c2] = graph[c2][c1] = d;
    }
    taken.clear(); taken.assign(C, false);
    for (auto i = 0; i < C; ++i) {
      if (taken[i]) continue;
      process(i);
      while (!pq.empty()) {
        auto tmp = pq.top(); pq.pop();
        auto u = tmp.second.first, v = tmp.second.second;
        if (!taken[v]) process(v);
        else graph[u].erase(v), graph[v].erase(u);
      }
    }
    while (Q--) {
      for (auto i = 0; i < C; ++i) taken[i] = false;
      int c1, c2; cin >> c1 >> c2; --c1; --c2;
      auto result = dfs(c1, c2);
      if (result < 0) cout << "no path\n";
      else cout << result << '\n';
    }
  }
}
