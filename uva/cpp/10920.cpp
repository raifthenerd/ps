#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int SZ, P; cin >> SZ >> P;
    if (SZ == 0 && P == 0) break;
    int root_col, root_row, root = 0, tmp = (SZ + 1) >> 1;
    while (P > root * root) ++root;
    bool odd_root = root & 1;
    if (odd_root) {
      root_col = root_row = tmp + (root >> 1);
    } else {
      root_col = root_row = tmp - (root >> 1);
      ++root_row;
    }
    tmp = root * root - P - root + 1;
    if (tmp < 0) {
      tmp = root + tmp - 1;
      if (odd_root) root_row -= tmp;
      else root_row += tmp;
    } else {
      if (odd_root) {
        root_row -= root - 1;
        root_col -= tmp;
      } else {
        root_row += root - 1;
        root_col += tmp;
      }
    }
    cout << "Line = " << root_row << ", column = " << root_col << ".\n";
  }
}
