#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string s;
  while (getline(cin, s)) {
    list<string> res;
    bool front = false;
    size_t curr = 0, next, home = 0, end = 0;
    while (true) {
      if (home != string::npos && home <= curr)
        home = s.find_first_of("[", curr);
      if (end != string::npos && end <= curr)
        end = s.find_first_of("]", curr);
      if (home == string::npos) {
        if (end == string::npos) break;
        else next = end;
      } else {
        if (end == string::npos) next = home;
        else next = min(home, end);
      }
      auto tmp = s.substr(curr, next - curr);
      if (front) res.push_front(tmp);
      else res.push_back(tmp);
      front = next == home;
      curr = next + 1;
    }
    auto tmp = s.substr(curr);
    if (front) res.push_front(tmp);
    else res.push_back(tmp);
    for (auto&& s : res) cout << s;
    cout << '\n';
  }
}
