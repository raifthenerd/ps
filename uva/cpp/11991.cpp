#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t n, m, k, v;
  while (cin >> n) {
    cin >> m;
    map<int, vector<int>> result;
    for (size_t i = 1; i <= n; ++i) {
      cin >> v; result[v].push_back(i);
    }
    while (m--) {
      cin >> k >> v;
      if (result.count(v) == 0 || result[v].size() < k) cout << "0\n";
      else cout << result[v][k-1] << '\n';
    }
  }
}
