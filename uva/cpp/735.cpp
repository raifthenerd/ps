#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  set<int> tmp {0, 50};
  for (auto i = 1; i <= 20; ++i)
    for (auto j = 1; j <= 3; ++j)
      tmp.insert(i*j);
  vector<int> scores;
  for (auto c : tmp) scores.push_back(c);
  while (true) {
    int s; cin >> s;
    if (s <= 0) {
      cout << "END OF OUTPUT\n";
      break;
    }
    int comb = 0, perm = 0;
    for (size_t a = 0; a < scores.size(); ++a) {
      for (size_t b = a; b < scores.size(); ++b) {
        for (size_t c = b; c < scores.size(); ++c) {
          if (s == scores[a] + scores[b] + scores[c]) {
            ++comb;
            if (a == b && b == c) ++perm;
            else if (a == b || b == c || c == a) perm += 3;
            else perm += 6;
          }
        }
      }
    }
    if (comb > 0) {
      cout << "NUMBER OF COMBINATIONS THAT SCORES " << s << " IS " << comb << ".\n";
      cout << "NUMBER OF PERMUTATIONS THAT SCORES " << s << " IS " << perm << ".\n";
    } else {
      cout << "THE SCORE OF " << s << " CANNOT BE MADE WITH THREE DARTS.\n";
    }
    cout << "**********************************************************************\n";
  }
}
