#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T, a, b, c, tmp;
  cin >> T;
  for (auto i = 1; i <= T; ++i) {
    cin >> a >> b >> c;
    cout << "Case " << i << ": ";
    if ((a-b) * (a-c) < 0) cout << a;
    else if ((b-a) * (b-c) < 0) cout << b;
    else if ((c-a) * (c-b) < 0) cout << c;
    cout << "\n";
  }
}
