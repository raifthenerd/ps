#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    string prev, curr; getline(cin, prev);
    if (prev == "END") break;
    int i = 0;
    while (++i) {
      curr = to_string(prev.size());
      if (prev == curr) break;
      prev = curr;
    }
    cout << i << '\n';
  }
}
