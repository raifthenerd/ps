#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int A, C; cin >> A;
    if (A == 0) break; cin >> C;
    int result = 0, prev = A, curr;
    while (C--) {
      cin >> curr;
      if (prev > curr) result += prev - curr;
      prev = curr;
    }
    cout << result << '\n';
  }
}
