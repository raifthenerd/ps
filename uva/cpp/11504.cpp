#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

vector<vi> graph;
vi dfs_num, dfs_low, parent;
vector<bool> visited;
stack<int> S;
int counter, result;

void ssc(int node) {
  S.push(node); visited[node] = true;
  dfs_num[node] = dfs_low[node] = ++counter;
  for (auto c : graph[node]) {
    if (dfs_num[c] == oo) ssc(c);
    if (visited[c]) dfs_low[node] = min(dfs_low[node], dfs_low[c]);
  }
  if (dfs_num[node] == dfs_low[node]) {
    while (true) {
      auto v = S.top(); S.pop(); visited[v] = false;
      parent[v] = node;
      if (v == node) break;
    }
    ++result;
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T;
  while (T--) {
    int n, m; cin >> n >> m;
    graph.clear(); graph.assign(n, {});
    while (m--) {
      int x, y; cin >> x >> y; graph[x-1].push_back(y-1);
    }
    dfs_num.clear(); dfs_num.assign(n, oo);
    dfs_low.clear(); dfs_low.assign(n, 0);
    parent.clear(); parent.assign(n, 0);
    visited.clear(); visited.assign(n, false);
    while (!S.empty()) S.pop(); counter = 0; result = 0;
    for (auto i = 0; i < n; ++i) if (dfs_num[i] == oo) ssc(i);
    visited.clear(); visited.assign(n, false);
    for (auto i = 0; i < n; ++i) {
      for (auto j : graph[i]) {
        auto ip = parent[i], jp = parent[j];
        if (ip != jp) visited[jp] = true;
      }
    }
    for (auto b : visited) if (b) --result;
    cout << result << '\n';
  }
}
