#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n;
  cin >> n;
  for (auto i = 0; i < n; ++i) {
    int f;
    cin >> f;
    int farmyard, n_animals, friendlines, premium = 0;
    for (auto j = 0; j < f; ++j) {
      cin >> farmyard >> n_animals >> friendlines;
      premium += farmyard * friendlines;
    }
    cout << premium << "\n";
  }
}
