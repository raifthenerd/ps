#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

bool grid[1000][1000]; int dist[1000][1000];

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int R, C; cin >> R >> C; if (R == 0 && C == 0) break;
    for (auto i = 0; i < R; ++i)
      for (auto j = 0; j < C; ++j)
        grid[i][j] = false, dist[i][j] = -1;
    int rows; cin >> rows;
    while (rows--) {
      int nrow, nbomb, ncol; cin >> nrow >> nbomb;
      while (nbomb--) cin >> ncol, grid[nrow][ncol] = true;
    }
    ii s, t; cin >> s.first >> s.second >> t.first >> t.second;
    queue<ii> q; q.push(s); dist[s.first][s.second] = 0;
    const vii diffs = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
    bool found = false;
    while (!found && !q.empty()) {
      auto x = q.front().first, y = q.front().second; q.pop();
      for (const ii& diff : diffs) {
        auto tmpx = x + diff.first, tmpy = y + diff.second;
        if (tmpx < R && tmpx >= 0 && tmpy < C && tmpy >= 0 &&
            !grid[tmpx][tmpy] && dist[tmpx][tmpy] < 0)
          q.push({tmpx, tmpy}), dist[tmpx][tmpy] = dist[x][y] + 1;
        if (tmpx == t.first && tmpy == t.second) {
          found = true; break;
        }
      }
    }
    cout << dist[t.first][t.second] << '\n';
  }
}
