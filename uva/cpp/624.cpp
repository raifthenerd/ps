#include <bits/stdc++.h>

using namespace std;

vector<bool> selection, result;
vector<int> cd;

size_t bt(size_t target, size_t curr, size_t idx) {
  if (curr == target || idx >= selection.size()) return curr;
  size_t tmp = 0;
  for (size_t i = 0; i < idx; ++i) if (selection[i]) tmp += cd[i];
  for (auto b : {true, false}) {
    selection[idx] = b;
    if (b) tmp += cd[idx];
    else tmp -= cd[idx];
    if (tmp <= target) {
      if (tmp > curr) {
        curr = tmp;
        result = selection;
      }
      curr = max(curr, bt(target, curr, idx + 1));
    }
  }
  return curr;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t N;
  while (cin >> N) {
    int t; cin >> t;
    cd.clear(); selection.clear(); result.clear();
    while (t--) {
      int x; cin >> x;
      cd.push_back(x); selection.push_back(false); result.push_back(false);
    }
    auto sum = bt(N, 0, 0);
    for (size_t i = 0; i < result.size(); ++i)
      if (result[i]) cout << cd[i] << ' ';
    cout << "sum:" << sum << '\n';
  }
}
