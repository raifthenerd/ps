#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    list<pair<int, int>> pops;
    while (n--) {
      int row, col, num; cin >> row >> col >> num;
      pops.push_back({5 * row + col, num});
    }
    int result[5], res_dist = numeric_limits<int>::max();
    for (auto a = 0; a < 25; ++a) {
      for (auto b = a+1; b < 25; ++b) {
        for (auto c = b+1; c < 25; ++c) {
          for (auto d = c+1; d < 25; ++d) {
            for (auto e = d+1; e < 25; ++e) {
              auto dist = 0;
              for (auto&& p : pops) {
                auto tmp = abs(p.first/5 - a/5) + abs(p.first%5 - a%5);
                tmp = min(tmp, abs(p.first/5 - b/5) + abs(p.first%5 - b%5));
                tmp = min(tmp, abs(p.first/5 - c/5) + abs(p.first%5 - c%5));
                tmp = min(tmp, abs(p.first/5 - d/5) + abs(p.first%5 - d%5));
                tmp = min(tmp, abs(p.first/5 - e/5) + abs(p.first%5 - e%5));
                dist += tmp * p.second;
              }
              if (dist < res_dist) {
                result[0] = a; result[1] = b; result[2] = c;
                result[3] = d; result[4] = e; res_dist = dist;
              }
            }
          }
        }
      }
    }
    cout << result[0];
    for (auto i = 1; i <= 4; ++i) cout << ' ' << result[i];
    cout << '\n';
  }
}
