#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int E; cin >> E; cin.get();
  vector<vi> graph; graph.assign(E, {});
  for (auto i = 0; i < E; ++i) {
    int n; cin >> n; graph[i].assign(n, 0);
    for (auto j = 0; j < n; ++j) cin >> graph[i][j];
  }
  int T; cin >> T;
  while (T--) {
    int s; cin >> s;
    if (graph[s].size() == 0) cout << "0\n";
    else {
      vi days; days.assign(E, -1);
      queue<int> q; q.push(s); days[s] = 0;
      auto boomsize = 1, boomday = 1, lastday = 0, tmp = 0;
      while (!q.empty()) {
        auto u = q.front(); q.pop();
        if (lastday == days[u]) ++tmp;
        else {
          if (boomsize < tmp) boomday = lastday, boomsize = tmp;
          lastday = days[u], tmp = 1;
        }
        for (auto v : graph[u]) {
          if (days[v] < 0) q.push(v), days[v] = days[u] + 1;
        }
      }
      if (boomsize < tmp) boomday = lastday, boomsize = tmp;
      cout << boomsize << ' ' << boomday << '\n';
    }
  }
}
