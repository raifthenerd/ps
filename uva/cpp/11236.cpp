#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(2);
  for (unsigned long long a = 1; a <= 500; ++a) {
    for (unsigned long long b = a; b <= 666; ++b) {
      for (unsigned long long c = b; c <= 1000; ++c) {
        if (a * b * c > 1000000 &&
            (1000000 * (a + b + c)) % (a * b * c - 1000000) == 0) {
          auto d = (1000000 * (a + b + c)) / (a * b * c - 1000000);
          if (c <= d && a + b + c + d <= 2000) {
            cout << static_cast<float>(a) / 100.0 << ' '
                 << static_cast<float>(b) / 100.0 << ' '
                 << static_cast<float>(c) / 100.0 << ' '
                 << static_cast<float>(d) / 100.0 << '\n';
          }
        }
      }
    }
  }
}
