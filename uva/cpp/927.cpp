#include <bits/stdc++.h>

using namespace std;

typedef unsigned long long ull;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  ull c[21] = {};
  while (T--) {
    for (auto i = 0; i < 21; ++i) c[i] = 0;
    ull d, k; cin >> d; for (k = 0; k <= d; ++k) cin >> c[k];
    cin >> d >> k;
    ull n = 0; while ((k-1)/d >= (n+1)*n/2) ++n;
    ull res = c[20];
    for (auto i = 19; i >= 0; --i) res = n * res + c[i];
    cout << res << '\n';
  }
}
