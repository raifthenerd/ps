#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int B, N;
    cin >> B >> N;
    if (B == 0 && N == 0) break;
    vector<int> R(B);
    for (auto i = 0; i < B; ++i) cin >> R[i];
    int D, C, V;
    for (auto i = 0; i < N; ++i) {
      cin >> D >> C >> V;
      R[D-1] -= V; R[C-1] += V;
    }
    bool possible = true;
    for (auto r : R) {
      if (r < 0) {
        possible = false;
        break;
      }
    }
    cout << (possible ? "S" : "N") << "\n";
  }
}
