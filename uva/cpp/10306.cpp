#include <bits/stdc++.h>

using namespace std;


int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n; cin >> n;
  while (n--) {
    int m, S; cin >> m >> S;
    int mem[S+1][S+1];
    for (auto i = 0; i <= S; ++i)
      for (auto j = 0; j <= S; ++j)
        mem[i][j] = 2 * S + 1;
    vector<pair<int, int>> coins;
    while (m--) {
      int x, y; cin >> x >> y; coins.push_back({x, y}); mem[x][y] = 1;
    }
    int result = 2 * S + 1;
    for (auto l = 0; l <= 3*S/2; ++l) {
      for (auto i = max(0, l - S); i <= min(S, l); ++i) {
        auto j = l - i;
        for (auto&& c : coins) {
          if (i >= c.first && j >= c.second) {
            mem[i][j] = min(mem[i][j], 1 + mem[i-c.first][j-c.second]);
          }
        }
        if (i*i + j*j == S*S) result = min(result, mem[i][j]);
      }
    }
    if (result > 2 * S) cout << "not possible\n";
    else cout << result << '\n';
  }
}
