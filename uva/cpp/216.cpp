#include <bits/stdc++.h>

using namespace std;

double adj[8][8], cache[8][256]; int n, travel[8][256];
double solve(int curr, unsigned int mask) {
  if (mask == (1U << n) - 1) return 0.0;
  if (cache[curr][mask] <= 0.0) {
    cache[curr][mask] = numeric_limits<double>::max();
    for (auto nxt = 0; nxt < n; ++nxt) {
      if (nxt == curr) continue;
      if (mask & (1U << nxt)) continue;
      auto tmp = adj[curr][nxt] + solve(nxt, mask | (1U << nxt));
      if (tmp < cache[curr][mask]) {
        cache[curr][mask] = tmp;
        travel[curr][mask] = nxt;
      }
    }
  }
  return cache[curr][mask];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(2);
  int T = 0;
  while (true) {
    cin >> n; if (n == 0) break;
    cout << "**********************************************************\n"
         << "Network #" << ++T << '\n';
    for (auto i = 0; i < 8; ++i)
      for (auto j = 0; j < 256; ++j)
        cache[i][j] = 0.0;
    vector<pair<int, int>> nodes;
    for (auto i = 0; i < n; ++i) {
      int x, y; cin >> x >> y; nodes.push_back({x, y});
    }
    for (auto i = 0; i < n; ++i) {
      adj[i][i] = 0.0;
      for (auto j = i+1; j < n; ++j) {
        auto xdiff = nodes[i].first - nodes[j].first,
             ydiff = nodes[i].second - nodes[j].second;
        adj[i][j] = adj[j][i] = sqrt(xdiff * xdiff + ydiff * ydiff);
      }
    }
    auto result = numeric_limits<double>::max();
    auto start = 0;
    for (auto i = 0; i < n; ++i) {
      auto tmp = solve(i, 1U << i);
      if (tmp < result) {
        result = tmp;
        start = i;
      }
    }
    auto mask = 1U << start;
    for (auto i = 1; i < n; ++i) {
      auto nxt = travel[start][mask];
      cout << "Cable requirement to connect "
           << '(' << nodes[start].first << ',' << nodes[start].second << ')'
           << " to "
           << '(' << nodes[nxt].first << ',' << nodes[nxt].second << ')'
           << " is " << adj[start][nxt] + 16.0 << " feet.\n";
      start = nxt; mask |= 1U << nxt;
    }
    cout << "Number of feet of cable required is "
         << result + 16.0 * (n - 1) << ".\n";
  }
}
