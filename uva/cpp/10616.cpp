#include <bits/stdc++.h>

using namespace std;

int N, D; vector<int> xs;
map<tuple<int, int, int>, long long> cache;
long long solve(int idx, int rem, int curr) {
  if (rem == 0) return curr == 0;
  if (idx == N) return 0;
  auto key = make_tuple(idx, rem, curr);
  if (cache.find(key) == cache.end())
    cache[key] = solve(idx+1, rem, curr) + solve(idx+1, rem-1, (curr + xs[idx]) % D);
  return cache[key];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (++T) {
    int Q; cin >> N >> Q; if (N == 0 && Q == 0) break;
    cout << "SET " << T << ":\n";
    xs.clear(); xs.assign(N, 0);
    for (auto i = 0; i < N; ++i) cin >> xs[i];
    for (auto i = 1; i <= Q; ++i) {
      int M; cin >> D >> M; cache.clear();
      cout << "QUERY " << i << ": " << solve(0, M, 0) << '\n';
    }
  }
}
