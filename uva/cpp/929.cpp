#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();
const vii diffs = {{-1, 0}, {1, 0}, {0, -1}, {0, 1}};
int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T; cin >> T;
  while (T--) {
    int N, M; cin >> N >> M;
    int grid[N][M], dist[N][M];
    REP(i, 0, N) REP(j, 0, M) cin >> grid[i][j], dist[i][j] = (oo >> 1);
    dist[0][0] = grid[0][0];
    priority_queue<pair<int, ii>,
                   vector<pair<int, ii>>,
                   greater<pair<int, ii>>> pq; pq.push({dist[0][0], {0, 0}});
    bool found = false;
    while (!found && !pq.empty()) {
      auto w = pq.top().first; auto u = pq.top().second; pq.pop();
      if (w > dist[u.first][u.second]) continue;
      TRAV(diff, diffs) {
        auto vx = u.first + diff.first, vy = u.second + diff.second;
        if (vx < 0 || vx >= N || vy < 0 || vy >= M) continue;
        if (dist[u.first][u.second] + grid[vx][vy] < dist[vx][vy]) {
          dist[vx][vy] = dist[u.first][u.second] + grid[vx][vy];
          pq.push({dist[vx][vy], {vx, vy}});
          if (vx == N - 1 && vy == M - 1) { found = true; break; }
        }
      }
    }
    cout << dist[N-1][M-1] << '\n';
  }
}
