#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n;
  bool first = true;
  while (cin >> n) {
    if (first)
      first = false;
    else
      cout << '\n';
    vector<string> names; map<string, int> result;
    string s; int val, m;
    for (auto i = 0; i < n; ++i) {
      cin >> s; names.push_back(s); result[s] = 0;
    }
    for (auto i = 0; i < n; ++i) {
      cin >> s >> val >> m;
      if (m > 0) {
        val /= m;
        result[s] -= val * m;
        for (auto j = 0; j < m; ++j) {
          cin >> s; result[s] += val;
        }
      }
    }
    for (auto&& name : names)
      cout << name << ' ' << result[name] << '\n';
  }
}
