#include <bits/stdc++.h>

using namespace std;

const int coins[5] = {1, 5, 10, 25, 50};
unsigned long long changes[5][30001] = {0};

unsigned long long solve(int idx, int charge) {
  if (idx == 0) return 1;
  if (changes[idx][charge] == 0) {
    for (auto i = 0; i <= charge/coins[idx]; ++i)
      changes[idx][charge] += solve(idx - 1, charge - i * coins[idx]);
  }
  return changes[idx][charge];
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n;
  while (cin >> n) {
    auto result = solve(4, n);
    cout << "There ";
    if (result == 1) cout << "is only 1 way";
    else cout << "are " << result << " ways";
    cout << " to produce " << n << " cents change.\n";
  }
}
