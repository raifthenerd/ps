#include <bits/stdc++.h>

using namespace std;

const int BLACK[32] = {-1, 3, 3, 2, 1, 3, 3, 2, 3, 2, 1, 3, 3, 2, 0, 3,
                        3, 1, 2, 3, 3, 0, 2, 3, 2, 3, 3, 0, 2, 3, 3, 1};
const int WHITE[16] = {-1, 3, 3, 2, 3, 2, 1, 3, 3, 1, 2, 3, 2, 3, 3, 0};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int fsm[2] = {0, 0};
    for (auto i = 0; i < 9; ++i) {
      char tmp; cin >> tmp;
      fsm[i & 1] = (fsm[i & 1] << 1) | (tmp == '1') ;
    }
    cout << max(BLACK[fsm[0]], WHITE[fsm[1]]) << '\n';
  }
}
