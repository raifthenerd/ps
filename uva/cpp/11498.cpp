#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int K, N, M, x, y;
  while (true) {
    cin >> K;
    if (K == 0) break;
    cin >> N >> M;
    for (auto i = 0; i < K; ++i) {
      cin >> x >> y;
      if (x == N || y == M) {
        cout << "divisa\n";
      } else {
        cout << (y >= M ? "N" : "S") << (x >= N ? "E" : "O") << "\n";
      }
    }
  }
}
