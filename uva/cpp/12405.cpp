#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int n; cin >> n;
    size_t result = 0;
    for (auto i = 0; i < n; ++i) {
      char c; cin >> c;
      if (c == '.') {
        if (++i < n) cin >> c;
        if (++i < n) cin >> c;
        ++result;
      }
    }
    cout << "Case " << t << ": " << result << '\n';
  }
}
