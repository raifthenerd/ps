#include <bits/stdc++.h>

using namespace std;

class SegmentTree {
 private:
  size_t N; vector<int> seg;
  size_t left(size_t p) { return p << 1; }
  size_t right(size_t p) { return 1 + (p << 1); }
  void build(const vector<int>& mem, size_t p, size_t L, size_t R) {
    if (L == R) seg[p] = mem[L];
    else {
      build(mem, left(p), L, (L + R) / 2);
      build(mem, right(p), (L + R) / 2 + 1, R);
      seg[p] = seg[left(p)] * seg[right(p)];
    }
  };
  void update(size_t p, size_t L, size_t R, size_t i, int v) {
    if (L > R || L > i || R < i) return;
    if (L == i && R == i) seg[p] = v;
    else {
      update(left(p), L, (L + R) / 2, i, v);
      update(right(p), (L + R) / 2 + 1, R, i, v);
      seg[p] = seg[left(p)] * seg[right(p)];
    }
  };
  int range_query(size_t p, size_t L, size_t R, size_t i, size_t j) {
    if (L > R || i > R || j < L) return 1;
    if (i == L && j == R) return seg[p];
    if (j <= (L + R) / 2)
      return range_query(left(p), L, (L + R) / 2, i, j);
    if (i >= (L + R) / 2 + 1)
      return range_query(right(p), (L + R) / 2 + 1, R, i, j);
    return range_query(left(p), L, (L + R) / 2, i, (L + R) / 2)
        * range_query(right(p), (L + R) / 2 + 1, R, (L + R) / 2 + 1, j);
  };
 public:
  SegmentTree(const vector<int>& mem) {
    N = mem.size();
    seg.assign(4 * N, 0);
    build(mem, 1, 0, N-1);
  };
  void update(size_t i, int v) {
    v = (v == 0) ? 0 : v / abs(v);
    update(1, 0, N-1, i-1, v);
  }
  char range_query(size_t i, size_t j) {
    auto res = range_query(1, 0, N-1, i-1, j-1);
    if (res == 1) return '+';
    else if (res == -1) return '-';
    return '0';
  };
};

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, K;
  while (cin >> N) {
    cin >> K;
    vector<int> Xs;
    while (N--) {
      int X; cin >> X;
      Xs.push_back((X == 0) ? 0 : X / abs(X));
    }
    SegmentTree st(Xs);
    while (K--) {
      char c; cin >> c;
      if (c == 'C') {
        int I, V; cin >> I >> V;
        st.update(I, V);
      } else if (c == 'P') {
        int I, J; cin >> I >> J;
        cout << st.range_query(I, J);
      }
    }
    cout << '\n';
  }
}
