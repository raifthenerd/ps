#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n, prev, curr;
  while (cin >> n) {
    bool jolly = true;
    vector<bool> diff(n-1, false);
    cin >> prev;
    for (auto i = 1; i < n; ++i) {
      cin >> curr;
      int d = abs(prev - curr) - 1;
      if (d < 0 || d >= n-1 || diff[d]) jolly = false;
      else diff[d] = true;
      prev = curr;
    }
    cout << (jolly ? "Jolly" : "Not jolly") << '\n';
  }
}
