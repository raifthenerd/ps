#include <bits/stdc++.h>

using namespace std;

int wind[10][1000], mem[10][1001];
const int INF = numeric_limits<int>::max() - 100;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N; cin >> N;
  while (N--) {
    int X; cin >> X; X /= 100;
    for (auto y = 9; y >= 0; --y) {
      for (auto x = 0; x < X; ++x) {
        cin >> wind[y][x]; mem[y][x] = INF;
      }
    }
    mem[0][0] = 0;
    for (auto x = 1; x <= X; ++x) {
      for (auto y = 0; y <= min(min(X - x, x), 9); ++y) {
        mem[y][x] = mem[y][x-1] + 30 - wind[y][x-1];
        if (y > 0) mem[y][x] = min(mem[y-1][x-1] + 60 - wind[y-1][x-1],
                                   mem[y][x]);
        if (y < 9) mem[y][x] = min(mem[y+1][x-1] + 20 - wind[y+1][x-1],
                                   mem[y][x]);
      }
    }
    cout << mem[0][X] << "\n\n";
  }
}
