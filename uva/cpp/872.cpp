#include <bits/stdc++.h>

using namespace std;

vector<char> nodes; map<char, vector<char>> adj; set<string> result;

void BT(string s, unsigned int mask) {
  if (mask == (1U << nodes.size()) - 1) {
    s.pop_back(); result.insert(s);
  } else {
    for (size_t i = 0; i < nodes.size(); ++i) {
      if (mask & (1U << i)) continue;
      bool valid = true;
      for (auto c : adj[nodes[i]]) {
        if (s.find(c) != s.npos) {
          valid = false; break;
        }
      }
      if (valid) BT(s + nodes[i] + ' ', mask | (1U << i));
    }
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    nodes.clear(); adj.clear(); result.clear();
    do  {
      char c; cin >> c; nodes.push_back(c);
    } while (cin.get() != '\n');
    do  {
      string s; cin >> s; adj[s[0]].push_back(s[2]);
    } while (cin.get() != '\n');
    BT("", 0);
    if (result.empty()) cout << "NO\n";
    else for (auto s : result) cout << s << '\n';
    if (T) cout << '\n';
  }
}
