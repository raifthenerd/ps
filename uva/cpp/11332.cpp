#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int u;
  while (true) {
    cin >> u;
    if (u == 0) break;
    auto tmp = u % 9;
    cout << (tmp > 0 ? tmp : 9) << "\n";
  }
}
