#include <bits/stdc++.h>

using namespace std;

double p, q, r, s, t, u;
const double f(double x) {
  return p*exp(-x)+q*sin(x)+r*cos(x)+s*tan(x)+t*x*x+u;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  cout << fixed << setprecision(4);
  while (cin >> p) {
    cin >> q >> r >> s >> t >> u;
    if (f(0.0) < 0.0 || f(1.0) > 0.0) cout << "No solution\n";
    else {
      double xl = 0.0, xu = 1.0, x;
      do  {
        x = 0.5 * (xl + xu);
        if (f(x) > 0.0) xl = x;
        else xu = x;
      } while (abs(xl - xu) >= 1e-7);
      cout << x << '\n';
    }
  }
}
