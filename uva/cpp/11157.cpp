#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int N, D; cin >> N >> D;
    vector<int> small[2];
    int result = 0, Mprev = 0; bool isodd = true;
    while (N--) {
      char S, tmp; int Mcurr; cin >> S >> tmp >> Mcurr;
      if (S == 'B') {
        auto res = 0;
        for (auto&& s : small) {
          auto sprev = Mprev;
          for (auto c : s) {
            res = max(res, c - sprev); sprev = c;
          }
          res = max(res, Mcurr - sprev);
          s.clear();
        }
        result = max(result, res);
        Mprev = Mcurr; isodd = true;
      } else {
        small[isodd].push_back(Mcurr); isodd = !isodd;
      }
    }
    auto res = 0;
    for (auto&& s : small) {
      auto sprev = Mprev;
      for (auto c : s) {
        res = max(res, c - sprev); sprev = c;
      }
      res = max(res, D - sprev);
      s.clear();
    }
    result = max(result, res);
    cout << "Case " << t << ": " << result << '\n';
  }
}
