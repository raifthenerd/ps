#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int i = 0;
  string s;
  while (++i) {
    cin >> s;
    if (s == "*") break;
    cout << "Case " << i << ": ";
    if (s == "Hajj") cout << "Hajj-e-Akbar\n";
    else if (s == "Umrah") cout << "Hajj-e-Asghar\n";
  }
}
