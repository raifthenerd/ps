#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    unsigned int n; cin >> n;
    if (n == 0) break;
    unsigned int a = 0, b = 0;
    unsigned int mask = 1; bool odd = true;
    for (auto idx = 0; idx < 32; ++idx) {
      if (n & mask) {
        if (odd) a |= mask;
        else b |= mask;
        odd = !odd;
      }
      mask <<= 1;
    }
    cout << a << ' ' << b << '\n';
  }
}
