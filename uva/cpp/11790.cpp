#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int N; cin >> N;
    vector<int> height(N), width(N), incr(N), decr(N);
    for (auto i = 0; i < N; ++i) cin >> height[i];
    for (auto i = 0; i < N; ++i) cin >> width[i];
    int A = 0, B = 0;
    for (auto i = 0; i < N; ++i) {
      for (auto j = 0; j < i; ++j) {
        if (height[j] < height[i]) incr[i] = max(incr[i], incr[j]);
        else if (height[j] > height[i]) decr[i] = max(decr[i], decr[j]);
      }
      incr[i] += width[i]; decr[i] += width[i];
      A = max(A, incr[i]); B = max(B, decr[i]);
    }
    cout << "Case " << t << ". ";
    if (A >= B) cout << "Increasing (" << A << "). Decreasing (" << B << ").\n";
    else cout << "Decreasing (" << B << "). Increasing (" << A << ").\n";
  }
}
