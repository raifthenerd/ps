#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, C = 0;
  while (cin >> N) {
    map<string, int> priority; map<string, set<string>> adj;
    while (N--) {
      string s; cin >> s; priority[s] = N;
    }
    int M; cin >> M;
    while (M--) {
      string a, b; cin >> a >> b; adj[b].insert(a);
    }
    cout << "Case #" << ++C << ": Dilbert should drink beverages in this order:";
    string result = "";
    auto cmp = [&](string l, string r) { return priority[l] < priority[r]; };
    priority_queue<string, vector<string>, decltype(cmp)> q(cmp);
    for (auto s : priority) if (adj[s.first].empty()) q.push(s.first);
    while (!q.empty()) {
      auto u = q.top(); q.pop(); priority.erase(u); adj.erase(u);
      for (auto s : priority) {
        auto it = adj[s.first].find(u);
        if (it != adj[s.first].end()) {
          adj[s.first].erase(it);
          if (adj[s.first].empty()) q.push(s.first);
        }
      }
      result = result + ' ' + u;
    }
    cout << result << ".\n\n";
  }
}
