#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int n; cin >> n; if (n == 0) break;
    multiset<long long> urn; long long reward = 0;
    while (n--) {
      int k; cin >> k;
      while (k--) {
        int bill; cin >> bill; urn.insert(bill);
      }
      auto highest = prev(urn.end());
      reward += *highest - *urn.begin();
      urn.erase(highest); urn.erase(urn.begin());
    }
    cout << reward << '\n';
  }
}
