#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  vector<int> xs, ps, mem, prevs;
  int x, i = 0;
  while (cin >> x) {
    auto it = lower_bound(mem.begin(), mem.end(), x);
    if (it == mem.begin()) ps.push_back(-1);
    else ps.push_back(prevs[distance(mem.begin(), it) - 1]);
    if (it == mem.end()) {
      mem.push_back(x); prevs.push_back(i);
    } else {
      *it = x; prevs[distance(mem.begin(), it)] = i;
    }
    xs.push_back(x); ++i;
  }
  cout << mem.size() << "\n-\n";
  stack<int> tmp;
  auto p = prevs.back();
  while (p >= 0) {
    tmp.push(xs[p]); p = ps[p];
  }
  while (!tmp.empty()) {
    cout << tmp.top() << '\n'; tmp.pop();
  }
}
