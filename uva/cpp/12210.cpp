#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T = 0;
  while (++T) {
    int B, S, tmp; cin >> B >> S; if (B == 0 && S == 0) break;
    cout << "Case " << T << ": ";
    vector<int> bachelor(B);
    for (auto i = 0; i < B; ++i) cin >> bachelor[i];
    for (auto i = 0; i < S; ++i) cin >> tmp;
    sort(bachelor.begin(), bachelor.end());
    if (B <= S) cout << "0\n";
    else cout << B - S << " " << bachelor.front() << '\n';
  }
}
