#include <bits/stdc++.h>

using namespace std;

char grid[100][100]; bool visited[100][100];

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  for (auto t = 1; t <= T; ++t) {
    int N; cin >> N;
    for (auto y = 0; y < N; ++y) {
      for (auto x = 0; x < N; ++x) {
        cin >> grid[y][x]; visited[y][x] = false;
      }
    }
    queue<pair<int, int>> q; int result = 0;
    for (auto y = 0; y < N; ++y) {
      for (auto x = 0; x < N; ++x) {
        if (visited[y][x] || grid[y][x] == '.') continue;
        bool sunk = true;
        q.push({y, x}); visited[y][x] = true;
        while (!q.empty()) {
          auto j = q.front().first, i = q.front().second; q.pop();
          if (grid[j][i] == 'x') sunk = false;
          if (i > 0 && !visited[j][i-1] && grid[j][i-1] != '.') {
            q.push({j, i-1}); visited[j][i-1] = true;
          }
          if (i + 1 < N && !visited[j][i+1] && grid[j][i+1] != '.') {
            q.push({j, i+1}); visited[j][i+1] = true;
          }
          if (j > 0 && !visited[j-1][i] && grid[j-1][i] != '.') {
            q.push({j-1, i}); visited[j-1][i] = true;
          }
          if (j + 1 < N && !visited[j+1][i] && grid[j+1][i] != '.') {
            q.push({j+1, i}); visited[j+1][i] = true;
          }
        }
        if (!sunk) ++result;
      }
    }
    cout << "Case " << t << ": " << result << '\n';
  }
}
