#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 1;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int set = 1, n; while (cin >> n) {
    cout << "Set #" << set++ << '\n';
    vi busyness(n, 0),  dist(n, oo), parent(n, -1); vector<vii> outgoing;
    REP(i, 0, n) cin >> busyness[i]; outgoing.assign(n, {});
    int r; cin >> r; while (r--) {
      int s, t; cin >> s >> t; --s; --t;
      auto d = busyness[t] - busyness[s]; outgoing[s].push_back({t, d * d * d});
    }
    if (n > 0) dist[0] = 0;
    REP(i, 1, n) REP(u, 0, n) TRAV(v, outgoing[u])
      if (dist[v.first] > dist[u] + v.second)
        dist[v.first] = dist[u] + v.second, parent[v.first] = u;
    int q; cin >> q; while (q--) {
      int dest; cin >> dest; --dest;
      auto result = dist[dest];
      bool question = dist[0] < 0 || result < 3 || result > (oo >> 1);
      while (!question && dest != 0) {
        auto d = busyness[dest] - busyness[parent[dest]];
        if (dist[dest] > dist[parent[dest]] + d * d * d) question = true;
        dest = parent[dest];
      }
      if (question) cout << "?\n"; else cout << result << '\n';
    }
  }
}
