#include <bits/stdc++.h>

using namespace std;

template<typename T> void print(T& q) {
  while(!q.empty()) {
    cout << q.top() << '\n';
    q.pop();
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, tmp; cin >> N;
  while (N--) {
    size_t B, SG, SB; cin >> B >> SG >> SB;
    priority_queue<int> green, blue;
    while (SG--) {
      cin >> tmp; green.push(tmp);
    }
    while (SB--) {
      cin >> tmp; blue.push(tmp);
    }
    bool end = false;
    while (!end) {
      end = true;
      if (green.size() == 0) {
        if (blue.size() == 0) cout << "green and blue died\n";
        else {cout << "blue wins\n"; print(blue);}
      } else {
        if (blue.size() == 0) {cout << "green wins\n"; print(green);}
        else {
          end = false;
          list<int> green_survs, blue_survs;
          int battle = min(B, min(green.size(), blue.size()));
          while (battle--) {
            int g = green.top(), b = blue.top(); green.pop(); blue.pop();
            if (g == b) continue;
            else if (g > b) green_survs.push_back(g - b);
            else blue_survs.push_back(b - g);
          }
          for (auto s : green_survs) green.push(s);
          for (auto s : blue_survs) blue.push(s);
        }
      }
    }
    if (N) cout << '\n';
  }
}
