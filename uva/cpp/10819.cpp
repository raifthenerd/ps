#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int m, n;
  while (cin >> m) { cin >> n;
    vector<int> mem(m+200+1, -1); mem[0] = 0;
    multimap<int, int> input;
    while (n--) {
      int p, f; cin >> p >> f; input.emplace(p, f);
    }
    for (auto it = input.begin(); it != input.end(); ++it) {
      auto p = it->first, f = it->second;
      for (auto i = m + 200; i >= p; --i) {
        if (mem[i - p] >= 0) mem[i] = max(mem[i], f + mem[i - p]);
      }
    }
    for (auto i = m+1; i <= min(2000, m+200); ++i) mem[i] = 0;
    auto result = 0; for (auto res : mem) result = max(result, res);
    cout << result << '\n';
  }
}
