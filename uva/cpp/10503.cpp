#include <bits/stdc++.h>

using namespace std;

size_t n, m;
int i[2], d[2];
vector<pair<int, int>> dominos;
vector<bool> used;

bool bt(size_t idx, int curr) {
  if (idx == n) return curr == d[0];
  for (size_t i = 0; i < used.size(); ++i) {
    if (!used[i]) {
      used[i] = true;
      if (curr == dominos[i].first) {
        if (bt(idx+1, dominos[i].second)) return true;
      } else if (curr == dominos[i].second) {
        if (bt(idx+1, dominos[i].first)) return true;
      }
      used[i] = false;
    }
  }
  return false;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    cin >> n; if (n == 0) break; cin >> m;
    cin >> i[0] >> i[1] >> d[0] >> d[1];
    dominos.clear(); used.clear();
    while (m--) {
      int p, q; cin >> p >> q; dominos.push_back({p, q}); used.push_back(false);
    }
    cout << (bt(0, i[1]) ? "YES\n" : "NO\n");
  }
}
