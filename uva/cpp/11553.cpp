#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    vector<vector<int>> M;
    M.assign(n, {}); for (auto i = 0; i < n; ++i) M[i].assign(n, 0);
    for (auto i = 0; i < n; ++i) {
      for (auto j = 0; j < n; ++j) {
        int x; cin >> x; M[i][j] = x;
      }
    }
    vector<int> bob; for (auto i = 0; i < n; ++i) bob.push_back(i);
    int result = numeric_limits<int>::max();
    do {
      auto tmp = 0;
      for (auto i = 0; i < n; ++i) tmp += M[i][bob[i]];
      if (tmp < result) result = tmp;
    } while (next_permutation(bob.begin(), bob.end()));
    cout << result << '\n';
  }
}
