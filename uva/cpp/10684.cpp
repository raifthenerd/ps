#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N; cin >> N; if (N == 0) break;
    int sum = 0, ans = 0;
    while (N--) {
      int b; cin >> b; sum += b;
      ans = max(ans, sum);
      if (sum < 0) sum = 0;
    }
    if (ans > 0) cout << "The maximum winning streak is " << ans << ".\n";
    else cout << "Losing streak.\n";
  }
}
