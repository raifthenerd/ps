#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t, n;
  cin >> t;
  for (auto i = 0; i < t; ++i) {
    cin >> n;
    cout << abs((315 * n + 36962) / 10 % 10) << "\n";
  }
}
