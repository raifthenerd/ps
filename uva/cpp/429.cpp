#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

bool connected(const string& lhs, const string& rhs) {
  if (SZ(lhs) != SZ(rhs)) return false;
  auto diff = 0;
  for (auto i = 0; i < SZ(lhs); ++i)
    if (lhs[i] != rhs[i]) ++diff;
  return diff == 1;
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int N; cin >> N;
  while (N--) {
    map<string, vector<string>> graph;
    map<string, int> distance;
    while (true) {
      string s; cin >> s; cin.get(); if (s == "*") break;
      graph[s] = {}; distance[s] = -1;
      for (auto& c : graph)
        if (connected(s, c.first))
          c.second.push_back(s), graph[s].push_back(c.first);
    }
    while (cin.peek() != '\n' && !cin.eof()) {
      string u, v; cin >> u >> v; cin.get(); cout << u << ' ' << v << ' ';
      queue<string> q; q.push(u);
      for (auto& c : distance) c.second = -1; distance[u] = 0;
      bool found = false;
      while (!found && !q.empty()) {
        auto u = q.front(); q.pop();
        for (const auto& c : graph[u]) {
          if (distance[c] < 0) {
            q.push(c); distance[c] = distance[u] + 1;
          }
          if (c == v) {
            found = true; break;
          }
        }
      }
      cout << distance[v] << '\n';
    }
    if (N) cout << '\n';
  }
}
