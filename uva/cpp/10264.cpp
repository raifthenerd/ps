#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned int N; cin >> N;
  while (!cin.eof()) {
    unsigned int n = 1 << N;
    vector<unsigned int> corners(n, 0);
    for (auto i = 0; i < n; ++i) {
      unsigned int tmp; cin >> tmp;
      for (auto j = 0; j < N; ++j)
        corners[i ^ (1 << j)] += tmp;
    }
    unsigned int maximum = 0;
    for (auto i = 0; i < n; ++i) {
      for (auto j = 0; j < N; ++j) {
        auto tmp = corners[i] + corners[i ^ (1 << j)];
        if (maximum < tmp) maximum = tmp;
      }
    }
    cout << maximum << '\n';
    cin >> N;
  }
}
