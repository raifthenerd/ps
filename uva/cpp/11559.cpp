#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N, B, H, W;
  while (cin >> N) {
    cin >> B >> H >> W;
    int p, a, m = B + 1;
    for (auto i = 0; i < H; ++i) {
      cin >> p;
      for (auto j = 0; j < W; ++j) {
        cin >> a;
        if (a >= N) m = min(N * p, m);
      }
    }
    if (m > B)
      cout << "stay home\n";
    else
      cout << m << "\n";
  }
}
