#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string cmd;
  map<int, int> cmds;
  while (true) {
    cin >> cmd; if (cmd == "#") break;
    int Qnum, period; cin >> Qnum >> period;
    cmds[Qnum] = period;
  }
  auto cmp = [](pair<int, int> left, pair<int, int> right) {
               if (left.second == right.second) return left.first > right.first;
               return left.second > right.second;
             };
  priority_queue<pair<int, int>, vector<pair<int, int>>, decltype(cmp)> q(cmp);
  for (auto&& c : cmds) q.push({c.first, c.second});
  int K; cin >> K;
  while (K--) {
    auto tmp = q.top(); q.pop();
    cout <<  tmp.first << '\n';
    q.push({tmp.first, tmp.second + cmds[tmp.first]});
  }
}
