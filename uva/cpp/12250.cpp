#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string s;
  int i = 0;
  while (++i) {
    cin >> s;
    if (s == "#") break;
    cout << "Case " << i << ": ";
    if (s == "HELLO") cout << "ENGLISH";
    else if (s ==  "HOLA") cout << "SPANISH";
    else if (s == "HALLO") cout << "GERMAN";
    else if (s == "BONJOUR") cout << "FRENCH";
    else if (s == "CIAO") cout << "ITALIAN";
    else if (s == "ZDRAVSTVUJTE") cout << "RUSSIAN";
    else cout << "UNKNOWN";
    cout << "\n";
  }
}
