#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int k, m;
  while (true) {
    cin >> k;
    if (k == 0) break;
    cin >> m;
    set<string> classes; string s; bool graduate = true;
    while (k--) {
      cin >> s; classes.insert(s);
    }
    int c, r;
    while (m--) {
      cin >> c >> r;
      while (c--) {
        cin >> s; r -= classes.count(s);
      }
      if (r > 0) graduate = false;
    }
    cout << (graduate ? "yes\n" : "no\n");
  }
}
