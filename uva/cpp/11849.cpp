#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  while (true) {
    int N, M; cin >> N >> M; if (N == 0 && M == 0) break;
    set<int> jack; int cd, result = 0;
    while (N--) {
      cin >> cd; jack.insert(cd);
    }
    while (M--) {
      cin >> cd;
      if (jack.find(cd) != jack.end()) ++result;
    }
    cout << result << endl;
  }
}
