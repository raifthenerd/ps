#include <bits/stdc++.h>

using namespace std;

const vector<int> primes = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
vector<int> ring;
vector<bool> used;
int N;

bool check(int p) {
  return binary_search(primes.begin(), primes.end(), p);
}
void bt(int idx) {
  if (idx == N) {
    if (check(ring.back() + 1)) {
      cout << 1;
      for (auto i = 1; i < N; ++i) cout << ' ' << ring[i];
      cout << '\n';
    }
  } else {
    for (auto i = 1; i <= N; ++i) {
      if (!used[i-1]) {
        ring[idx] = i; used[i-1] = true;
        if (check(ring[idx-1] + ring[idx])) bt(idx + 1);
        used[i-1] = false;
      }
    }
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int t = 0;
  while (cin >> N) {
    if (t) cout << '\n';
    cout << "Case " << ++t << ":\n";
    ring.clear(); ring.assign(N, 1);
    used.clear(); used.assign(N, false); used[0] = true;
    bt(1);
  }
}
