#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 1;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int M; cin >> M; if (M == 0) break;
    string O, D; cin >> O >> D;
    map<string, vi> dist; dist[O].assign(26, oo); dist[D].assign(26, oo);
    map<string, vector<pair<string, string>>> graph;
    while (M--) {
      string I[2], P; cin >> I[0] >> I[1] >> P;
      REP(i, 0, 2) graph[I[i]].push_back({I[1-i], P});
    }
    TRAV(g, graph) dist[g.first].assign(26, oo);
    priority_queue<pair<int, pair<string, int>>,
                   vector<pair<int, pair<string, int>>>,
                   greater<pair<int, pair<string, int>>>> pq;
    REP(c, 0, 26) pq.push({dist[O][c] = 0, {O, c}});
    while (!pq.empty()) {
      auto u = pq.top().second; auto w = pq.top().first; pq.pop();
      if (w > dist[u.first][u.second]) continue;
      TRAV(v, graph[u.first]) {
        if (u.second != v.second[0] - 'a' &&
            dist[u.first][u.second] + SZ(v.second) < dist[v.first][v.second[0] - 'a']) {
          dist[v.first][v.second[0] - 'a'] = dist[u.first][u.second] + SZ(v.second);
          pq.push({dist[v.first][v.second[0] - 'a'], {v.first, v.second[0] - 'a'}});
        }
      }
    }
    auto result = oo; REP(c, 0, 26) result = min(result, dist[D][c]);
    if (result == oo) cout << "impossivel\n"; else cout << result << '\n';
  }
}
