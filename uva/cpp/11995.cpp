#include <bits/stdc++.h>

using namespace std;

template<typename T> T see_top(queue<T> c) {return c.front();}
template<typename T> T see_top(stack<T> c) {return c.top();}
template<typename T> T see_top(priority_queue<T> c) {return c.top();}

template<typename Container>
bool action(Container& c, int cmd, int x) {
  if (cmd == 1) c.push(x);
  else if (cmd == 2) {
    if (c.empty() || see_top(c) != x)
      return false;
    c.pop();
  }
  return true;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int n;
  while (cin >> n) {
    bool is_st = true, is_qu = true, is_pq = true;
    stack<int> st;
    queue<int> qu;
    priority_queue<int> pq;
    while (n--) {
      int cmd, x; cin >> cmd >> x;
      is_st = is_st && action(st, cmd, x);
      is_qu = is_qu && action(qu, cmd, x);
      is_pq = is_pq && action(pq, cmd, x);
    }
    if (!is_st && !is_qu && !is_pq)
      cout << "impossible\n";
    else if ((is_st && is_qu) || (is_st && is_pq) || (is_pq && is_qu))
      cout << "not sure\n";
    else if (is_st)
      cout << "stack\n";
    else if (is_qu)
      cout << "queue\n";
    else if (is_pq)
      cout << "priority queue\n";
  }
}
