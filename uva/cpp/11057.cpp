#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N;
  while (cin >> N) {
    set<int> books;
    while (N--) {
      int price; cin >> price; books.insert(price);
    }
    int M; cin >> M;
    auto it = books.lower_bound(M/2);
    while (it != books.end()) {
      if (books.find(M - *it) != books.end()) break;
      ++it;
    }
    cout << "Peter should buy books whose prices are "
         << M - *it << " and " << *it << ".\n\n";
  }
}
