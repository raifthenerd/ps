#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  while (T--) {
    int tmp, n; cin >> n;
    vector<int> inst(n);
    char c;
    for (auto i = 0; i < n; ++i) {
      cin >> c;
      switch (c) {
        case 'L':
          inst[i] = -1;
          cin.seekg(4, ios_base::cur);
          break;
        case 'R':
          inst[i] = 1;
          cin.seekg(5, ios_base::cur);
          break;
        case 'S':
          cin.seekg(7, ios_base::cur);
          cin >> tmp;
          inst[i] = inst[--tmp];
          break;
      }
    }
    tmp = 0;
    for (auto v : inst) tmp += v;
    cout << tmp << '\n';
  }
}
