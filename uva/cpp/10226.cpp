#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int T; cin >> T;
  string s; getline(cin, s); getline(cin, s);
  cout << fixed << setprecision(4);
  while (T--) {
    map<string, unsigned long long> result; unsigned long long overall = 0;
    while (true) {
      getline(cin, s); if (s == "") break;
      if (result.count(s) == 0) result[s] = 0; ++result[s];
      ++overall;
    }
    auto d_overall = static_cast<double>(overall);
    for (auto&& c : result)
      cout << c.first << ' ' << (100 * c.second) / d_overall << '\n';
    if (T) cout << '\n';
  }
}
