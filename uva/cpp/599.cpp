#include <bits/stdc++.h>

using namespace std;

void dfs(map<char, set<char>>& edges, set<char>& visited, char curr) {
  if (visited.count(curr) > 0) return;
  visited.insert(curr);
  for (auto c : edges[curr]) dfs(edges, visited, c);
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int N; cin >> N; cin.get();
  while (N--) {
    map<char, set<char>> edges;
    while (true) {
      string s; cin >> s; if (s.front() == '*') break;
      edges[s[1]].insert(s[3]);
      edges[s[3]].insert(s[1]);
    }
    set<char> nodes;
    while (true) {
      char c; cin >> c; nodes.insert(c);
      if (cin.peek() == ',') cin.get();
      else break;
    }
    int trees = 0, acorns = 0; set<char> visited;
    for (auto c : nodes) {
      if (edges[c].empty()) ++acorns;
      else if (visited.count(c) == 0) ++trees;
      dfs(edges, visited, c);
    }
    cout << "There are "
         << trees << " tree(s) and "
         << acorns << " acorn(s).\n";
  }
}
