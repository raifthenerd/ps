#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max();

vector<vi> graph;
vi dfs_num, dfs_low;
vector<bool> visited;
stack<int> S;
int counter, result;

void ssc(int node) {
  S.push(node); visited[node] = true;
  dfs_num[node] = dfs_low[node] = ++counter;
  for (auto c : graph[node]) {
    if (dfs_num[c] == oo) ssc(c);
    if (visited[c]) dfs_low[node] = min(dfs_low[node], dfs_low[c]);
  }
  if (dfs_num[node] == dfs_low[node]) {
    ++result;
    while (true) {
      auto v = S.top(); S.pop(); visited[v] = false;
      if (v == node) break;
    }
  }
}

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  while (true) {
    int N, M; cin >> N >> M; if (N == 0 && M == 0) break;
    graph.clear(); graph.assign(N, {});
    while (M--) {
      int V, W, P; cin >> V >> W >> P; --V; --W; --P;
      graph[V].push_back(W); if (P) graph[W].push_back(V);
    }
    dfs_num.clear(); dfs_num.assign(N, oo);
    dfs_low.clear(); dfs_low.assign(N, 0);
    visited.clear(); visited.assign(N, false);
    while (!S.empty()) S.pop(); counter = 0; result = 0;
    for (auto i = 0; i < N; ++i) if (dfs_num[i] == oo) ssc(i);
    cout << (result == 1) << '\n';
  }
}
