#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  string s;
  while (cin.peek() != EOF) {
    int scores[2] = {0, 0};
    for (auto i = 0; i < 2; ++i) {
      getline(cin, s);
      for (auto&& c : s) {
        if (islower(c))
          scores[i] += (c - 'a' + 1) % 9;
        else if (isupper(c))
          scores[i] += (c - 'A' + 1) % 9;
      }
      scores[i] %= 9;
      if (scores[i] == 0) scores[i] = 9;
    }
    if (scores[0] > scores[1]) iter_swap(scores, scores+1);
    printf("%.2f %%\n", (100.0 * scores[0]) / scores[1]);
  }
}
