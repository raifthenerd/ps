#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0;
  while (true) { ++T;
    int n; cin >> n; if (n < 0) break;
    vi energy(n, 0);
    vector<vi> outgoings; outgoings.assign(n, {});
    REP(i, 0, n) {
      int m; cin >> energy[i] >> m;
      while (m--) { int j; cin >> j; outgoings[i].push_back(--j); }
    }
    vi dist(n, -oo); vector<bool> reach(n, false);
    dist[0] = 100; reach[n-1] = true;
    REP(i, 1, n) REP(u, 0, n) {
      TRAV(v, outgoings[u]) {
        reach[u] = reach[u] || reach[v];
        if (dist[u] <= 0) continue;
        dist[v] = max(dist[v], dist[u] + energy[v]);
      }
    }
    auto cycle = false;
    REP(u, 0, n) {
      if (dist[u] <= 0) continue;
      TRAV(v, outgoings[u])
        cycle |= reach[v] && (dist[v] < dist[u] + energy[v]);
    }
    cout << (dist.back() > 0 || cycle ? "winnable\n": "hopeless\n");
  }
}
