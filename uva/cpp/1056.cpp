#include <bits/stdc++.h>
using namespace std;

#define REP(i, a, b) for (int i = a; i < (b); ++i)
#define TRAV(x, c) for (auto& x : c)
#define ALL(c) (c).begin(), (c).end()
#define SZ(c) static_cast<int>((c).size())

typedef long long ll;
typedef pair<int, int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

const int oo = numeric_limits<int>::max() >> 2;
unordered_map<string, unordered_map<string, int>> net;

int main() {
  ios::sync_with_stdio(false); cin.tie(0);
  int T = 0; while (++T) {
    int P, R; cin >> P >> R; if (P == 0 && R == 0) break;
    net.clear(); while (R--) {
      string a, b; cin >> a >> b; net[a][b] = net[b][a] = 1;
    }
    TRAV(a, net) TRAV(b, net) {
      if (net[a.first].count(b.first) == 0)
        net[a.first][b.first] = (a.first == b.first) ? 0 : oo;
    }
    TRAV(k, net) TRAV(i, net) TRAV(j, net) {
      auto keyk = k.first, keyi = i.first, keyj = j.first;
      net[keyi][keyj] = min(net[keyi][keyj], net[keyi][keyk] + net[keyk][keyj]);
    }
    auto result = 0;
    TRAV(i, net) TRAV(j, net) result = max(result, net[i.first][j.first]);
    cout << "Network " << T << ": ";
    if (SZ(net) < P || result == oo) cout << "DISCONNECTED"; else cout << result;
    cout << "\n\n";
  }
}
