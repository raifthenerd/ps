#!/bin/bash

for i in $(seq 1 17; seq 100 132); do
    mkdir -p $i; cd $i
    for j in $(seq 0 99); do
        wget https://uva.onlinejudge.org/external/$i/$(($i*100+j)).pdf
    done
    gs -sDEVICE=pdfwrite \
       -dCompatibilityLevel=1.4 \
       -dPDFSETTINGS=/default \
       -dNOPAUSE -dQUIET -dBATCH \
       -dDetectDuplicateImages \
       -dCompressFonts=true \
       -r150 \
       -sOutputFile="$i"xx.pdf \
       *.pdf
    mv "$i"xx.pdf ..; cd ..; rm -rf $i
done
