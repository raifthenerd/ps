#include <iostream>
#include <list>

std::list<int> primes = {2, 3};

void gen_prime_until(int n) {
  int m = primes.back()+2;
  while (m < n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int num_divisor(int n) {
  if (n == 1) return 1;
  gen_prime_until(n);
  int result = 1;
  int m = n;
  for (auto p : primes) {
    if (p > n) break;
    int tmp = 0;
    while (!(m%p)) {
      m /= p;
      ++tmp;
    }
    result *= ++tmp;
  }
  return result;
}

int main() {
  std::ios::sync_with_stdio(false);
  int tri = 3, i = 3;
  while (num_divisor(tri) <= 500) tri += i++;
  std::cout << tri << std::endl;
}
