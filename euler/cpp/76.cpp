#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long mem[100][50] = {};
  for (auto i = 2; i <= 100; ++i) {
    for (auto j = 1; j <= i/2; ++j) {
      mem[i-1][j-1] = 1;
      for (auto k = j; k <= i/2; ++k) {
        mem[i-1][j-1] += mem[i-j-1][k-1];
      }
    }
  }
  unsigned long long result = 0;
  for (auto j = 0; j < 50; ++j) result += mem[99][j];
  cout << result << endl;
}
