#include <bits/stdc++.h>

using namespace std;

vector<unsigned long long> primes = {2, 3};

bool check_prime(unsigned long long n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned long long n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

unsigned long long phi(unsigned long long n) {
  unsigned long long result = n;
  for (auto p : primes) {
    if (n % p == 0) {
      result /= p; result *= p-1;
      do {
        n /= p;
      } while (n % p == 0);
    }
    if (p > n) break;
  }
  return result;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long p = 3;
  while (true) {
    p += 2; gen_prime_until(p);
    if (p != primes.back()) continue;
    for (auto idx = 2 << (primes.size() - 3);
         idx < 2 << (primes.size() - 2); ++idx) {
      unsigned long long tmp_num = p - 1, tmp_denom = p;
      auto tmp = idx;
      auto it = primes.cbegin();
      string s;
      while (tmp > 0) {
        if (tmp & 1) {
          tmp_num *= *it - 1;
          tmp_denom *= *it;
        }
        tmp >>= 1; ++it;
      }
      if (94744 * tmp_num < 15499 * tmp_denom) {
        unsigned long long d = 1;
        while (94744 * tmp_num * d >= 15499 * (tmp_denom * d - 1)) ++d;
        cout << d * tmp_denom << endl;
        return 0;
      }
    }
  }
}
