#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long lower = 10203040506070809,
                     upper = 19293949596979899;
  lower = static_cast<unsigned long long>(sqrt(lower));
  upper = static_cast<unsigned long long>(sqrt(upper));
  for (unsigned long long n = lower; n <= upper; ++n) {
    auto tmp = to_string(n * n);
    bool found = true;
    for (auto i = 0; found && i <= 16; i += 2) {
      found = tmp[i] - '0' == (i/2)+1;
    }
    if (found) {
      cout << 10 * n << endl; return 0;
    }
  }
}
