#include <iostream>
#include <list>


int main() {
  std::ios::sync_with_stdio(false);
  std::list<int> primes = {2, 3};
  int n = 5;
  while (primes.size() < 10001) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(n%p)) {
        is_prime = false;
        break;
      }
      if (p*p > n) break;
    }
    if (is_prime) primes.push_back(n);
    n += 2;
  }
  std::cout << primes.back() << std::endl;
}
