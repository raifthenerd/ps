#include <iostream>
#include <cstdint>
int main() {
  // = _{40}C_{20}
  std::ios::sync_with_stdio(false);
  uint64_t result = 1;
  for (auto i = 21; i <= 40; i = i+2) {
    result *= i;
  }
  result *= 1024;
  for (auto i = 2; i <= 10; ++i) {
    result /= i;
  }
  std::cout << result << std::endl;
}
