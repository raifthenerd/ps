#include <iostream>

int main() {
  // simple mathematics; no programming required
  // 1~19   : 36 + 70 = 106
  // 1~99   : 106 + 10*46 + 8*36 = 854
  // 1~999  : 854 + 100*(36+9*7) + 9*99*3 + 9*(36+70+10*46+8*36) = 21113
  // 1~1000 : 21113 + 11 = 21124
  std::ios::sync_with_stdio(false);
  std::cout << 21124 << std::endl;
}
