#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  auto result = 0, n = 1;
  while (n * log10(9) > n - 1) {
    for (unsigned long long base = 1; base < 10; ++base) {
      auto x = n * log10(base);
      if (n - 1 <= x && x < n) ++result;
    }
    ++n;
  }
  cout << result << endl;
}
