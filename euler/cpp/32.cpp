#include <iostream>
#include <set>
#include <algorithm>

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  int s[9] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
  std::set<int> products = {7254};
  do {
    int product = 1000*s[0]+100*s[1]+10*s[2]+s[3];
    int multiplicand = 10*s[4]+s[5], multiplier = 100*s[6]+10*s[7]+s[8];
    if (product == multiplicand * multiplier) {
      products.insert(product);
    } else {
      multiplicand = s[4];
      multiplier += 1000*s[5];
      if (product == multiplicand * multiplier) products.insert(product);
    }
  } while (std::next_permutation(s, s+9));
  int sum = 0;
  for (auto product : products) sum += product;
  std::cout << sum << std::endl;
}
