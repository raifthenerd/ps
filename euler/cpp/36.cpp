#include <iostream>
#include <vector>
#include <string>

template <typename Container>
bool is_palin(Container c) {
  auto foo = c.cbegin();
  auto bar = c.crbegin();
  do {
    if (*foo != *bar) return false;
  } while (++foo != c.cend() && ++bar != c.crend());
  return true;
}

int main() {
  std::ios::sync_with_stdio(false);
  uint result = 0;
  for (uint n = 1; n < 1000000; ++n) {
    std::string s = std::to_string(n);
    if (is_palin(s)) {
      auto tmp = n;
      std::vector<bool> b;
      while (tmp) {
        b.push_back(tmp&1);
        tmp >>= 1;
      }
      if (is_palin(b)) result += n;
    }
  }
  std::cout << result << std::endl;
}
