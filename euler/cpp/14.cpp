#include <iostream>

const int64_t N = 1000000;
int mem[N] = {1, 1};

int collatz_length(int64_t n) {
  int64_t m = n;
  int tmp = 0;
  while (m != 1) {
    if (m < N && mem[m]) break;
    if (m%2)
      m = 3*m+1;
    else
      m /= 2;
    ++tmp;
  }
  mem[n] = mem[m] + tmp;
  return mem[n];
}

int main() {
  std::ios::sync_with_stdio(false);
  int result_val = 1;
  int result_idx = 1;
  for (auto n = 1; n < N; ++n) {
    int tmp = collatz_length(n);
    if (tmp > result_val) {
      result_val = tmp;
      result_idx = n;
    }
  }
  std::cout << result_idx << std::endl;
}
