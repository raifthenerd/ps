#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);
  const uint64_t base = 10000000000l;
  uint64_t result = 0;
  for (auto i = 1; i <= 1000; ++i) {
    uint64_t tmp = i;
    for (auto j = 1; j < i; ++j) tmp = (tmp*i)%base;
    result = (result+tmp)%base;
  }
  std::cout << result << std::endl;
}
