#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int result = 0;
  for (auto N = 2; N < 10000; ++N) {
    double sqrtN = sqrt(N);
    auto period = 0, tmp = static_cast<int>(sqrtN);
    if (tmp * tmp == N) continue;
    auto init = make_pair(tmp, 1), chain = init;
    do {
      ++period;
      chain.second = (N - chain.first * chain.first) / chain.second;
      auto k = static_cast<int>((sqrtN + chain.first) / chain.second);
      chain.first = k * chain.second - chain.first;
    } while (init.first != chain.first || init.second != chain.second);
    if (period & 1) ++result;
  }
  cout << result << endl;
}
