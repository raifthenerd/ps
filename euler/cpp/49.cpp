#include <iostream>
#include <algorithm>
#include <list>
#include <string>

std::list<int> primes = {2, 3};

bool is_prime(int m) {
  for (auto p : primes) {
    if (!(m%p)) return false;
    if (p*p > m) return true;
  }
  return true;
}

void gen_prime_until(int n) {
  auto m = primes.back()+2;
  while (m < n) {
    if (is_prime(m)) primes.push_back(m);
    m += 2;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  gen_prime_until(10000);
  while (primes.front() < 1000) primes.pop_front();
  auto pb = primes.cbegin(), pe = primes.cend();
  for (auto p : primes) {
    if (p == 1487) continue;
    for (auto i = 2; i < (10000-p)/2; i += 2) {
      if (std::binary_search(pb, pe, p+i) &&
          std::binary_search(pb, pe, p+2*i)) {
        auto foo = std::to_string(p);
        auto fb = foo.cbegin(), fe = foo.cend();
        auto bar = std::to_string(p+i);
        auto baz = std::to_string(p+2*i);
        if (std::is_permutation(fb, fe, bar.begin()) &&
            std::is_permutation(fb, fe, baz.begin())) {
          std::cout << foo << bar << baz << std::endl;
          return 0;
        }
      }
    }
  }
}
