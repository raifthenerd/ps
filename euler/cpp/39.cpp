#include <iostream>

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  auto num_sol = 3, result = 120;
  for (auto p = 3; p <= 1000; ++p) {
    auto n = 0;
    for (auto a = 1; a <= p/3; ++a) {
      for (auto b = a+1; b <= p/2; ++b) {
        auto c = p-a-b;
        if (a*a+b*b == c*c) ++n;
      }
    }
    if (n > num_sol) {
      num_sol = n;
      result = p;
    }
  }
  std::cout << result << std::endl;
}
