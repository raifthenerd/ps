#include <bits/stdc++.h>

using namespace std;

vector<size_t> primes = {2, 3};

void gen_prime_until(size_t n) {
  auto m = primes.back()+2;
  while (m <= n) {
    auto is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t p = 56003;
  while (true) {
    p += 2;
    gen_prime_until(pow(10, 1 + static_cast<int>(log10(p))));
    if (binary_search(primes.begin(), primes.end(), p)) {
      for (auto i = '0'; i <= '2'; ++i) {
        auto ps = to_string(p);
        vector<int> digits; size_t loc = -1;
        while (true) {
          loc = ps.find(i, loc + 1);
          if (loc == string::npos) break;
          digits.push_back(loc);
        }
        int bdd = 1 << digits.size();
        for (auto j = 1; j < bdd; ++j) {
          vector<int> subdigits; auto tmp = j;
          for (size_t k = 0; k < digits.size(); ++k) {
            if (tmp & 1) {
              subdigits.push_back(digits[k]);
            }
            tmp >>= 1;
          }
          auto n = 1;
          for (auto digit : digits) ps[digit] = i;
          for (auto k = i + 1; k <= '9'; ++k) {
            for (auto digit : subdigits) ps[digit] = k;
            if (binary_search(primes.begin(), primes.end(), stoi(ps))) ++n;
          }
          if (n >= 8) {
            cout << p << '\n';
            return 0;
          }
        }
      }
    }
  }
}
