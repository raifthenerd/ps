#include <iostream>
#include <string>
#include <algorithm>

int main() {
  std::ios::sync_with_stdio(false);
  int ms[5];
  auto n = 0, m = 0, base = 10;
  auto found = false;
  while (!found) {
    if (n >= base) base *= 10;
    m = base + n++;
    for (auto i = 0; i < 5; ++i) ms[i] = (i+2)*m;
    auto tmp = std::to_string(m);
    auto tb = tmp.cbegin(), te = tmp.cend();
    found = true;
    for (auto i = 0; i < 5; ++i) {
      auto tmpi = std::to_string(ms[i]);
      if (!std::is_permutation(tb, te, tmpi.cbegin())) {
        found = false;
        break;
      }
    }
  }
  std::cout << m << std::endl;
}
