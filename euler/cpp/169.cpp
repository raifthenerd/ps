#include <bits/stdc++.h>

using namespace std;

unsigned long long f(unsigned long long N) {
  if (N == 0) return 1;
  auto prefix = N, m = 0ULL, n = 0ULL;
  while (!(prefix & 1)) {
    ++m; prefix >>= 1;
  }
  while (prefix & 1) {
    ++n; prefix >>= 1;
  }
  return f(prefix)*(1+(n-1)*m) + f(prefix<<1)*m;
}

unsigned long long f(unsigned long long N, unsigned long long m) {
  auto prefix = N, n = 0ULL;
  while (prefix & 1) {
    ++n; prefix >>= 1;
  }
  return f(prefix)*(1+(n-1)*m) + f(prefix<<1)*m;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned long long N = 1;
  for (auto i = 0; i < 25; ++i) N *= 5;
  cout << f(N, 25) << endl;
}
