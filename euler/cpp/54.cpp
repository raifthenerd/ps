#include <iostream>
#include <algorithm>

enum ranks {HIGH_CARD, ONE_PAIR, TWO_PAIRS, THREE_OF_A_KIND, STRAIGHT, FLUSH,
            FULL_HOUSE, FOUR_OF_A_KIND, STRAIGHT_FLUSH};

void get_hands(int nums[], bool same_suits, int hand[]) {
  bool is_straight = nums[0] & nums[1] & nums[2] & nums[3] & nums[12];
  for (auto i = 0; i <= 8; ++i) {
    bool tmp = nums[i];
    for (auto j = i+1; j <= i+4; ++j) {
      tmp &= nums[j];
    }
    if (tmp) {
      is_straight = true;
      break;
    }
  }

  int high = -1, pair_first = -1, pair_next = -1, max_pair = -1;
  for (auto i = 12; i >= 0; --i) {
    if (high < 0 && nums[i] > 0) high = i;
    if (nums[i] > 2) {
      max_pair = nums[i];
      pair_first = i;
    } else if (nums[i] == 2) {
      if (max_pair < 2) {
        max_pair = 2;
        pair_first = i;
      } else {
        pair_next = i;
      }
    }
  }

  if (same_suits && is_straight) {
    hand[0] = STRAIGHT_FLUSH;
    hand[1] = high;
  } else if (max_pair == 4) {
    hand[0] = FOUR_OF_A_KIND;
    hand[1] = pair_first;
  } else if (max_pair == 3 && pair_next >= 0) {
    hand[0] = FULL_HOUSE;
    hand[1] = pair_first;
    hand[2] = pair_next;
  } else if (same_suits) {
    hand[0] = FLUSH;
    hand[1] = high;
  } else if (is_straight) {
    hand[0] = STRAIGHT;
    hand[1] = high;
  } else if (max_pair == 3) {
    hand[0] = THREE_OF_A_KIND;
    hand[1] = pair_first;
  } else if (max_pair == 2 && pair_next >= 0) {
    hand[0] = TWO_PAIRS;
    hand[1] = pair_first;
    hand[2] = pair_next;
  } else if (max_pair == 2) {
    hand[0] = ONE_PAIR;
    hand[1] = pair_first;
  } else {
    hand[0] = HIGH_CARD;
    hand[1] = high;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  char s[2];
  int result = 0;
  int nums[13];
  bool same_suits;
  int hands[2][3];
  while (std::cin.good()) {
    for (auto player = 0; player < 2; ++player) {
      std::fill(hands[player], hands[player]+3, 0);
      std::fill(nums, nums+13, 0);
      same_suits = true;
      auto suit = 0;
      for (auto i = 0; i < 5; ++i) {
        std::cin >> s;
        switch (s[0]) {
          case 'T':
            ++nums[8]; break;
          case 'J':
            ++nums[9]; break;
          case 'Q':
            ++nums[10]; break;
          case 'K':
            ++nums[11]; break;
          case 'A':
            ++nums[12]; break;
          default:
            ++nums[s[0]-'2'];
        }
        if (i == 0) suit = s[1];
        else if (suit != s[1]) same_suits = false;
        get_hands(nums, same_suits, hands[player]);
      }
    }
    for (auto i = 0; i < 3; ++i) {
      auto tmp = hands[0][i] - hands[1][i];
      if (tmp != 0) {
        if (tmp > 0) ++result;
        break;
      }
    }
  }
  std::cout << result << std::endl;
}
