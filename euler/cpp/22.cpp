#include <iostream>
#include <string>
#include <list>

int alphavalue(const std::string &s) {
  int val = 0;
  for (auto c : s) {
    val += c-'A'+1;
  }
  return val;
}

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  std::string s;
  std::list<std::string> ss;
  while (std::cin.good()) {
    std::getline(std::cin, s, '\"');
    std::getline(std::cin, s, '\"');
    if (!s.empty()) ss.push_back(s);
  }
  ss.sort();
  int idx = 1, result = 0;
  for (auto s : ss) result += idx++ * alphavalue(s);
  std::cout << result << std::endl;
}
