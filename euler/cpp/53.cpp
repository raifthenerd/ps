#include <iostream>
#include <cmath>

int main() {
  std::ios::sync_with_stdio(false);
  int result = 0;
  for (auto n = 1; n <= 100; ++n) {
    for (auto r = 0; r <= n; ++r) {
      float tmp = 0.0;
      for (auto i = 0; i < r; ++i) tmp += std::log10(n-i)-std::log10(i+1);
      if (tmp > 6.0) ++result;
    }
  }
  std::cout << result << std::endl;
}
