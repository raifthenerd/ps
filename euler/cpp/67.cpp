#include <iostream>

const int N = 100;

int mem[N][N] = {};

int main() {
  std::ios::sync_with_stdio(false);
  for (auto i = 0; i < N; ++i) {
    for (auto j = 0; j <= i; ++j) {
      std::cin >> mem[i][j];
    }
  }
  for (auto i = N-2; i >= 0; --i) {
    for (auto j = 0; j <= i; ++j) {
      mem[i][j] += std::max(mem[i+1][j], mem[i+1][j+1]);
    }
  }
  std::cout << mem[0][0] << std::endl;
}
