#include <iostream>
#include <string>
#include <algorithm>

int main() {
  std::ios::sync_with_stdio(false);
  int result = 0;
  char m[20] = {}, mrev[20] = {};
  for (auto n = 1; n < 10000; ++n) {
    auto lychrel = true;
    auto sig = 0, tmp = n;
    for (auto i = 0; i < 20; ++i) {
      m[i] = tmp%10;
      tmp = tmp/10;
      if (tmp > 0) ++sig;
    }
    for (auto i = 0; i < 50; ++i) {
      for (auto j = 0; j < 20; ++j) mrev[j] = j <= sig ? m[sig - j] : 0;
      char carry = 0;
      for (auto j = 0; j < 20; ++j) {
        auto tmp = m[j] + mrev[j] + carry;
        m[j] = tmp%10;
        carry = tmp/10;
      }
      if (m[sig+1] > 0) ++sig;
      bool palin = true;
      for (auto j = 0; j <= sig; ++j) {
        if (m[j] != m[sig-j]) {
          palin = false; break;
        }
      }
      if (palin) {
        lychrel = false;
        break;
      }
    }
    if (lychrel) ++result;
  }
  std::cout << result << std::endl;
}
