#include <iostream>
#include <cmath>
#include <set>

int main() {
  std::ios::sync_with_stdio(false);
  std::set<double> tmp = {};
  for (auto a = 100; a >= 2; --a) {
    for (auto b = 2; b <= 100; ++b) {
      tmp.insert(b*std::log2(a));
    }
  }
  std::cout << tmp.size() << std::endl;
}
