#include <iostream>
#include <algorithm>
#include <list>

std::list<uint> primes = {2, 3};

int main() {
  std::ios::sync_with_stdio(false);
  uint m = 5;
  while (true) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) {
      primes.push_back(m);
    } else {
      bool counter = true;
      for (uint i = 1; i <= static_cast<uint>(std::sqrt(m/2)); ++i) {
        if (std::binary_search(primes.begin(), primes.end(), m-2*i*i)) {
          counter = false;
          break;
        }
      }
      if (counter) {
        std::cout << m << std::endl;
        return 0;
      }
    }
    m += 2;
  }
}
