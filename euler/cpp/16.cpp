#include <iostream>

int main() {
  std::ios::sync_with_stdio(false);
  char result[303] = {};
  result[301] = 1;
  for (auto i = 0; i < 1000; ++i) {
    char carry = 0;
    for (auto j = 301; j >= 0 || carry; --j) {
      auto tmp = 2*result[j]+carry;
      result[j] = tmp%10;
      carry = tmp/10;
    }
  }
  int sum = 0;
  for (auto i = 0; i < 302; ++i) {
    sum += result[i];
  }
  std::cout << sum << std::endl;
}
