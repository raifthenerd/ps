#include <bits/stdc++.h>

using namespace std;

/*
  28433*2^7830457 mod 10^10
  = 2^10 * (28433*2^7830447 mod 5^10)
  = 2^10 * (28433*2^17947 mod 5^10)  [Fermat's little thm; phi(5^10)=4*5^9]
*/

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  size_t b = 1, result = 28433, bdd = 7830457;
  for (auto i = 0; i < 10; ++i) b *= 5;
  bdd %= 4*b/5; bdd -= 10;
  while (bdd--) {
    result <<= 1; result %= b;
  }
  cout << 1024 * result + 1 << endl;
}
