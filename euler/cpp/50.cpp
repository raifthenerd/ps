#include <iostream>
#include <list>
#include <algorithm>

std::list<int> primes = {2, 3};

void gen_prime_until(int n) {
  int m = primes.back()+2;
  while (m < n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int main() {
  std::ios::sync_with_stdio(false);
  const int million = 1000000;
  gen_prime_until(million);
  auto len = 21, result = 953;
  auto pb = primes.cbegin(), pe = primes.cend();
  auto p = 2;
  for (auto it1 = pb; it1 != pe; ++it1) {
    auto it2 = it1;
    auto l = 1, tmp = *it2;
    do {
      p = *(++it2); tmp += p; ++l;
      if (std::binary_search(pb, pe, tmp) && l > len) {
        len = l; result = tmp;
        if (million - result < p) {
          std::cout << result << std::endl;
          return 0;
        }
      }
    } while (tmp < million);
  }
}
