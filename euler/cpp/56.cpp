#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  char n[200];
  int result = 1;
  for (auto a = 1; a < 100; ++a) {
    for (auto b = 1; b < 100; ++b) {
      auto sig = 0, tmp = a;
      for (auto i = 0; i < 200; ++i) {
        n[i] = tmp % 10;
        tmp = tmp/10;
        if (tmp > 0) ++sig;
      }
      for (auto i = 0; i < b; ++i) {
        char carry = 0;
        for (auto j = 0; j < 200; ++j) {
          tmp = n[j] * a + carry;
          n[j] = tmp%10;
          carry = tmp/10;
        }
        while (n[sig + 1] > 0) ++sig;
      }
      tmp = 0;
      for (auto i = 0; i < 200; ++i) tmp += n[i];
      result = max(result, tmp);
    }
  }
  cout << result << '\n';
}
