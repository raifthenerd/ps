#include <bits/stdc++.h>

using namespace std;

const string PUNC = " !\"'(),.?:;";

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  vector<int> msg; char key[4];
  while (!cin.eof()) {
    int d; cin >> d; msg.push_back(d);
    cin.get(); cin.peek();
  }
  for (auto a = 'a'; a <= 'z'; ++a) {
    key[0] = a;
    for (auto b = 'a'; b <= 'z'; ++b) {
      key[1] = b;
      for (auto c = 'a'; c <= 'z'; ++c) {
        key[2] = c;
        auto i = 0, sum = 0;
        string s; bool good = true;
        for (char m : msg) {
          auto ch = static_cast<char>(m ^ key[i++]);
          if ((ch >= 'a' && ch <= 'z') ||
              (ch >= 'A' && ch <= 'Z') ||
              (ch >= '0' && ch <= '9') ||
              PUNC.find_first_of(ch) != string::npos) s += ch;
          else {
            good = false; break;
          };
          i %= 3;
          sum += ch;
        }
        // key is "god"
        if (good) cout << key << " (" << sum << "): " << s << '\n';
      }
    }
  }
}
