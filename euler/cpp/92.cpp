#include <bits/stdc++.h>

using namespace std;

const int N = 10000000;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int result = 0;
  for (auto n = 2; n < N; ++n) {
    unsigned long long chain = n;
    while (true) {
      auto s_chain = to_string(chain);
      chain = 0;
      for (auto c : s_chain) chain += (c-'0') * (c-'0');
      if (chain == 89) {
        ++result; break;
      } else if (chain == 1) break;
    }
  }
  cout << result << endl;
}
