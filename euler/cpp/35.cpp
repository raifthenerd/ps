#include <iostream>
#include <algorithm>
#include <list>

std::list<uint> primes = {2, 3};

void gen_prime_until(uint n) {
  uint m = primes.back()+2;
  while (m < n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  uint result = 0;
  gen_prime_until(1000000);
  uint tmp = 1;
  for (auto p : primes) {
    if (tmp * 10 < p) tmp *= 10;
    bool cyclic = true;
    auto i = p;
    do {
      i = (i/10)+tmp*(i%10);
      if (!std::binary_search(primes.begin(), primes.end(), i)) {
        cyclic = false;
        break;
      }
    } while (i != p);
    if (cyclic) ++result;
  }
  std::cout << result << std::endl;
}
