#include <iostream>

int n2(int budget) {
  if (budget < 0)
    return 0;
  else
    return 1 + n2(budget - 2);
}
int n5(int budget) {
  if (budget < 0)
    return 0;
  else
    return n2(budget) + n5(budget - 5);
}
int n10(int budget) {
  if (budget < 0)
    return 0;
  else
    return n5(budget) + n10(budget - 10);
}
int n20(int budget) {
  if (budget < 0)
    return 0;
  else
    return n10(budget) + n20(budget - 20);
}
int n50(int budget) {
  if (budget < 0)
    return 0;
  else
    return n20(budget) + n50(budget - 50);
}
int n100(int budget) {
  if (budget < 0)
    return 0;
  else
    return n50(budget) + n100(budget - 100);
}
int n200(int budget) {
  if (budget < 0)
    return 0;
  else
    return n100(budget) + n200(budget - 200);
}

int main() {
  // simple mathematics; no programming required
  std::ios::sync_with_stdio(false);
  std::cout << n200(200) << std::endl;
}
