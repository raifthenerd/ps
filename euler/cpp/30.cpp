#include <iostream>
#include <cmath>

int main() {
  std::ios::sync_with_stdio(false);
  auto result = 0;
  for (auto n = 2; n <= 354294; ++n) {
    auto tmp = n, val = n;
    while (tmp >= 10) {
      val -= std::pow(tmp % 10, 5);
      tmp /= 10;
    }
    val -= std::pow(tmp, 5);
    if (!val) result += n;
  }
  std::cout << result << std::endl;
}
