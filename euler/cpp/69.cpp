#include <bits/stdc++.h>

using namespace std;

const int N = 1000000;
list<unsigned int> primes = {2, 3};

bool check_prime(unsigned int n) {
  if (n == 1) return false;
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned int n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

double log_phi_div_n(unsigned int n) {
  double result = 0;
  for (auto p : primes) {
    if (n % p == 0) {
      result += log(p-1) - log(p);
      do {
        n /= p;
      } while (n % p == 0);
    }
    if (p > n) break;
  }
  return result;
}


int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  gen_prime_until(N);
  double val = -log(3); auto nmax = 6;
  for (auto n = 11; n <= N; ++n) {
    auto tmp = log_phi_div_n(n);
    if (tmp < val) {
      nmax = n; val = tmp;
    }
  }
  cout << nmax << endl;
}
