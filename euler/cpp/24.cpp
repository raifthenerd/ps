#include <iostream>
#include <string>
#include <algorithm>

int main() {
  std::ios::sync_with_stdio(false);
  std::string s = "0123456789";
  for (auto i = 1; i < 1000000; ++i) {
    std::next_permutation(s.begin(), s.end());
  }
  std::cout << s << std::endl;
}
