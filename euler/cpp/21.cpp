#include <iostream>
#include <list>

std::list<int> primes = {2, 3};

void gen_prime_until(int n) {
  int m = primes.back()+2;
  while (m < n) {
    bool is_prime = true;
    for (auto p : primes) {
      if (!(m%p)) {
        is_prime = false;
        break;
      }
      if (p*p > m) break;
    }
    if (is_prime) primes.push_back(m);
    m += 2;
  }
}

uint64_t sum_divisor(int n) {
  if (n == 1) return 1;
  gen_prime_until(n);
  uint64_t result = 1;
  int m = n;
  for (auto p : primes) {
    if (p > n) break;
    int tmp = 1;
    while (!(m%p)) {
      m /= p;
      tmp *= p;
    }
    if (tmp > 1) result *= (tmp*p-1)/(p-1);
  }
  return result;
}

int main() {
  std::ios::sync_with_stdio(false);
  int vals[10000] = {};
  uint64_t sum = 0;
  for (auto i = 1; i <= 10000; ++i) vals[i-1] = sum_divisor(i)-i;
  for (auto i = 1; i <= 10000; ++i) {
    for (auto j = i+1; j <= 10000; ++j) {
      if (vals[i-1] == j && i == vals[j-1]) sum += i+j;
    }
  }
  std::cout << sum << std::endl;
}
