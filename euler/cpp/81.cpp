#include <bits/stdc++.h>

using namespace std;

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int matrix[80][80];
  for (auto i = 0; i < 80; ++i) {
    for (auto j = 0; j < 80; ++j) {
      int val; char tmp;
      if (j) cin >> tmp;
      cin >> val; matrix[i][j] = val;
    }
  }
  for (auto i = 1; i < 80; ++i) {
    matrix[i][0] += matrix[i-1][0];
    matrix[0][i] += matrix[0][i-1];
  }
  for (auto n = 1; n <= 79; ++n) {
    for (auto i = 1; i <= n; ++i) {
      matrix[n+1-i][i] += min(matrix[n+1-i][i-1],matrix[n-i][i]);
    }
  }
  for (auto n = 78; n >= 1; --n) {
    for (auto i = 1; i <= n; ++i) {
      matrix[80-i][79-n+i] += min(matrix[80-i-1][79-n+i],matrix[80-i][78-n+i]);
    }
  }
  cout << matrix[79][79] << endl;
}
