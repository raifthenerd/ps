#include <bits/stdc++.h>

using namespace std;

int gcd(int a, int b) {
  if (a > b) swap(a, b);
  if (a == 0) return b;
  return gcd(b%a, a);
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  int result = 0;
  for (auto d = 4; d <= 12000; ++d) {
    auto l = d/3+1, u = (d+1)/2-1;
    for (auto n = l; n <= u; ++n) {
      if (gcd(n, d) == 1) ++result;
    }
  }
  cout << result << endl;
}
