#include <bits/stdc++.h>

using namespace std;

list<unsigned int> primes = {2, 3};

bool check_prime(unsigned long n) {
  for (auto p : primes) {
    if (!(n%p)) return false;
    if (p*p > n) break;
  }
  return true;
}

void gen_prime_until(unsigned int n) {
  auto m = primes.back() + 2;
  while (m <= n) {
    if (check_prime(m)) primes.push_back(m);
    m += 2;
  }
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(NULL);
  unsigned int sidelen = 3, p = 1, nprimes = 0;
  while (true) {
    for (auto i = 0; i < 3; ++i) {
      p += sidelen - 1;
      gen_prime_until(static_cast<unsigned int>(sqrt(p)) + 1);
      if (check_prime(p)) ++nprimes;
    }
    p += sidelen - 1;
    if (nprimes * 10 <= 2 * sidelen - 1) {
      cout << sidelen << endl; return 0;
    }
    sidelen += 2;
  }
}
