#!/usr/bin/env bash

usage() {
    echo "usage: $0 [python|cpp] prob_num"
    exit 1
}

PYTHON='python3.6'
CXX='g++-7'
CXXFLAGS='-O2 -Wall -lm -static -std=gnu++17 -DONLINE_JUDGE -DBOJ'
TEST_INPUT='in.txt'
TEST_OUTPUT='out.txt'
TMP=`mktemp --suffix=.txt`

if [ $# != 2 ]; then
    usage
elif [ $1 == "python" ]; then
    $PYTHON -c "import py_compile; py_compile.compile(r'python/$2.py')"
    time $PYTHON python/$2.py <$TEST_INPUT >$TMP
    rm -rf python/__pycache__
elif [ $1 == "cpp" ]; then
    EXEC=`mktemp`; $CXX $CXXFLAGS -o $EXEC cpp/$2.cpp
    time $EXEC <$TEST_INPUT >$TMP
    rm -f $EXEC
else
    usage
fi
diff $TEST_OUTPUT $TMP
rm -f $TMP
